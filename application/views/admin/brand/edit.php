<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Danh mục
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="<?php echo site_url('admin/brand')?>">Hãng</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('msg')) { ?>
                <div class="alert alert-success" id="success-alert">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
                    <strong>Success! </strong>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            <?php } ?>
        <div class="box box-primary">            
            <div class="box-header with-border">
                <h3 class="box-title">Cập nhật</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="" method="POST" enctype="multipart/form-data">
                <div class="nav-tabs-custom">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name">Tên</label>
                                    <input type="text" class="form-control" required="" id="name" name="name" value="<?php echo $name ?>" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="name">Danh mục cha</label>
                                    <select name="parent_id" class="select2 form-control">
                                        <option value="0"></option>
                                        <?php $parents = getAllCategories();?>
                                        <?php if(count($parents) > 0) { ?>
                                            <?php foreach ($parents as $key => $parent) {
                                                $selected = ($parent->id == $parent_id)? ' selected ' : '';
                                                echo '<option '.$selected.' value="'.$parent->id.'">'.$parent->name.'</option>';
                                            } ?>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>                        
                    </div>
                    <!-- /.tab-content -->
                </div>


                <div class="box-footer">
                    <button type="submit" name="save" class="btn btn-primary" value="1">Lưu</button>
                    <a href="<?php echo site_url('admin/category') ?>" class="btn btn-default">Hủy</a>
                    <input type="hidden" name="pid" value="<?php echo $id?>" />
                </div>
            </form>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
