<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Slide
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Slide</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('msg')) { ?>
                <div class="alert alert-success" id="success-alert">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
                    <strong>Success! </strong>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            <?php } ?>
        <div class="box box-primary">            
            <div class="box-header with-border">
                <h3 class="box-title">Cập nhật Slide</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="" method="POST" enctype="multipart/form-data">
                <div class="box-body">
                    <div class="form-group">
                        <?php echo form_label('Ảnh', 'feature_image'); ?>
                        <?php echo form_error('feature_image', '<em class="error">', '</em>'); ?> 
                        <input type="file" class="form-control filestyle" data-buttonName="btn-primary" name="feature_image" id="feature_image" value="" data-buttonBefore="true"> 
                    </div>
                    <div class="form-group">
                        <img id="thumbPreview" src="<?php echo site_url($feature_image) ?>" style="width: 200px;height: auto;" />
                    </div>
                </div>
                <?php
                $title = unserialize($title);
                $slogan = unserialize($slogan);
                $link = unserialize($link);
                ?>
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab"><img title="Tiếng Việt" alt="Tiếng Việt" src="<?php echo site_url() ?>/skins/images/Vietnam.png" /></a></li>
                        <li><a href="#tab_2" data-toggle="tab"><img title="Tiếng Anh" alt="Tiếng Anh" src="<?php echo site_url() ?>/skins/images/United-States.png" /></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="title_vn">Tiêu đề</label>
                                    <input type="text" class="form-control" required="" id="title_vn" name="title_vn" value="<?php echo $title['vn']?>">
                                </div>

                                <div class="form-group">
                                    <label for="slogan_vn">Slogan</label>
                                    <input type="text" class="form-control" required="" id="slogan_vn" name="slogan_vn" value="<?php echo $slogan['vn']?>">
                                </div>
                                <div class="form-group">
                                    <label for="link_vn">Link</label>
                                    <input type="text" class="form-control" required="" id="link_vn" name="link_vn" value="<?php echo $link['vn']?>">
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="title_en">Tiêu đề</label>
                                    <input type="text" class="form-control" id="title_en" name="title_en" value="<?php echo $title['en']?>">
                                </div>

                                <div class="form-group">
                                    <label for="slogan_en">Slogan</label>
                                    <input type="text" class="form-control" id="slogan_en" name="slogan_en" value="<?php echo $slogan['en']?>">
                                </div>
                                <div class="form-group">
                                    <label for="link_en">Link</label>
                                    <input type="text" class="form-control" id="link_en" name="link_en" value="<?php echo $link['en']?>">
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>


                <div class="box-footer">
                    <button type="submit" name="save" class="btn btn-primary" value="1">Lưu</button>
                    <a href="<?php echo site_url('admin/slide') ?>" class="btn btn-default">Hủy</a>
                    <input type="hidden" name="pid" value="<?php echo $id?>" />
                </div>
            </form>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
