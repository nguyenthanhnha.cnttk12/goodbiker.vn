<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Đối tác
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Đối tác</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('msg')) { ?>
                <div class="alert alert-success" id="success-alert">
                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
                    <i class="fa fa-check-square-o" aria-hidden="true"></i>

                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            <?php } ?>
        <div class="box box-primary">            
            <div class="box-header with-border">
                <h3 class="box-title">Cập nhật</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="" method="POST" enctype="multipart/form-data">
                <div class="box-body">
                    <div class="form-group">
                        <?php echo form_label('Logo', 'feature_image'); ?>
                        <?php echo form_error('feature_image', '<em class="error">', '</em>'); ?> 
                        <input type="file" class="form-control filestyle" data-buttonName="btn-primary" name="feature_image" id="feature_image" value="" data-buttonBefore="true"> 
                    </div>
                    <div class="form-group">
                        <img id="thumbPreview" src="<?php echo site_url($feature_image) ?>" style="width: 200px;height: auto;" />
                    </div>
                    <div class="form-group">
                        <label for="title_vn">Công ty</label>
                        <input type="text" class="form-control" required="" id="name" name="name" value="<?php echo $name?>">
                    </div>
                    <div class="form-group">
                        <label for="link">Link</label>
                        <input type="text" class="form-control" required="" id="link" name="link" value="<?php echo $link?>">
                    </div>
                </div>
                
                <div class="box-footer">
                    <button type="submit" name="save" class="btn btn-primary" value="1">Lưu</button>
                    <a href="<?php echo site_url('admin/sponsor') ?>" class="btn btn-default">Hủy</a>
                    <input type="hidden" name="pid" value="<?php echo $id?>" />
                </div>
            </form>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
