<section class="main-content">
    <div class="content-wrapper product-detal-page">
        <div class="container">
            <div class="breadcrumb hidden-sm hidden-xs"><?php echo $breadcrumbs; ?></div>
            <div class="row">
                <div class="col-md-4  col-sm-6  col-xs-6">
                    <!-- <div class="product-detail-image">
                            <img src="<?php //echo $featured_image->path ?>" alt="<?php// echo $product->name; ?>">
                    </div>	 -->
                    <div class="sp-loading"><img src="<?php echo site_url('assets/smoothproducts/css/sp-loading.gif') ?>" alt=""><br>LOADING IMAGES</div>
                    <div class="sp-wrap">
                        <a href="<?php echo $featured_image->path ?>"><img src="<?php echo $featured_image->path ?>"></a>
                        <?php
foreach ($other_images as $key => $image) {
	echo '<a href="' . $image->path . '"><img src="' . $image->path . '"></a>';
}
?>
                    </div>
                </div>
                <div class="col-md-8 col-sm-6  col-xs-6">
                    <div class="product-summary">
                        <div class="product-detail-brand"><h3><span class="label label-danger"><?php echo $product->brand; ?></span></h3></div>
                        <h1 class="product-category-heading"><?php echo $product->name; ?></h1>
                        <div class="product-summary-details">
                            <div class="product-detail-price">
                                <del><?php echo number_format($product->price, 0, ',', '.'); ?> ₫</del>
                                <h3><?php echo number_format($this->product_model->SalesProductByID($product->id), 0, ',', '.'); ?> ₫</h3>
                            </div>
                            <div class="product-buying">
                                <a class="btn btn-lg btn-warning add-to-cart hidden-sm hidden-xs" data-productid="<?php echo $product->id; ?>"><i class="fa fa-cart-plus"></i> Thêm vào giỏ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="visible-xs visible-sm text-center">
                    <a class="btn btn-warning add-to-cart visible-xs visible-sm" data-productid="<?php echo $product->id; ?>"><i class="fa fa-cart-plus"></i> Thêm vào giỏ</a>
                </div>
        </div>
    </div>
    <div class="content-wrapper">
        <div class="container">
            <div class="row ">
                <div class="product-detail-description">
                    <h2 class="product-detail-description-heading">Mô tả sản phẩm <span class="hidden-sm hidden-xs"><?php echo $product->name ?></span></h2>
                    <div class="product-detail-description-tab"><?php echo $product->description; ?></div>
                </div>
            </div>
        </div>
    </div>
</section>