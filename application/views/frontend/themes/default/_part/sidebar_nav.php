

    <style>
            /* This only works with JavaScript,
               if it's not present, don't show loader */
            body{
                position: relative;
            }
            .loader { display: none; position: absolute;height: 100%;width: 100%; left: 0; top: 0; opacity: 0.5; z-index: 1001; background-color: #333;}
            .loader1 {
              border: 16px solid #f3f3f3;
              border-radius: 50%;
              border-top: 16px solid #3498db;
              position: fixed;
              left: 50%;
              top: 50%;
              width: 120px;
              height: 120px;
              -webkit-animation: spin 2s linear infinite; /* Safari */
              animation: spin 2s linear infinite;
            }

            /* Safari */
            @-webkit-keyframes spin {
              0% { -webkit-transform: rotate(0deg); }
              100% { -webkit-transform: rotate(360deg); }
            }

            @keyframes spin {
              0% { transform: rotate(0deg); }
              100% { transform: rotate(360deg); }
            }
            .c2uiAC {
    margin-top: 10px;
    padding-bottom: 20px;
    }
            .c1vuTH {
    margin-top: 8px;
    zoom: 1;
}
.c1vuTH::after, .c1vuTH::before {
    content: " ";
    display: table;
}
.c30Om7 {
    float: left;
    display: block;
    width: 70px;
    height: 30px;
    padding: 0 7px;
    text-align: left;
    color: #404040;
    border: 1px solid #a8a8a8;
    border-radius: 3px;
    outline: 0;
    background-color: #fff;
    -webkit-box-shadow: 0 1px 1px 0 #ececec;
    box-shadow: 0 1px 1px 0 #ececec;
    -webkit-transition: all .3s linear;
    -o-transition: all .3s linear;
    transition: all .3s linear;
    min-width: auto;
    -moz-appearance: textfield;
  }
  .c1DHiF {
    float: left;
    width: 10px;
    text-align: center;
    line-height: 30px;
    color: gray;
}
.ant-btn:not(.ant-btn-circle):not(.ant-btn-circle-outline).ant-btn-icon-only {
    padding-left: 8px;
    padding-right: 8px;
}
.c3R9mX {
    float: left;
    height: 30px;
    padding: 0 7px;
        padding-right: 7px;
        padding-left: 7px;
    border-radius: 3px;
    outline: 0;
    margin-left: 4px;
}
.ant-btn-primary {
    color: #fff;
    background-color: #f57224;
    border-color: #f57224;
}
.ant-btn {
    display: inline-block;
    margin-bottom: 0;
    font-weight: 500;
    text-align: center;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    background-image: none;
    border: 1px solid transparent;
        border-top-color: transparent;
        border-right-color: transparent;
        border-bottom-color: transparent;
        border-left-color: transparent;
    white-space: nowrap;
    line-height: 1.15;
    padding: 0 15px;
    font-size: 13px;
    border-radius: 2px;
    height: 28px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    -webkit-transition: all .3s cubic-bezier(.645,.045,.355,1);
    -o-transition: all .3s cubic-bezier(.645,.045,.355,1);
    transition: all .3s cubic-bezier(.645,.045,.355,1);
    position: relative;
    color: #404040;
    background-color: #fff;
    border-color: #a8a8a8;
}
        </style>
    <div class="a21"></div>
    <div id="cssmenu" class="sidebar">
        <h3 class="product-list-navigation-heading" id="aaa">Danh mục</h3>
        <ul class="list-unstyled sidemenu">
            <?php
$c = new stdClass;
$c->listing = '';

//list category
foreach ($category_lineage as &$c->category) {
	if ($c->category->parent_id == 0) {
		$c->listing .= '<li class="nav-menu level1 fly">' . recursive_ul($c->category, $current_category) . '</li>';
	}
}
//array id_brand_checked
$id_brand = $this->input->get('id_brand');
if (isset($id_brand)) {
	$idbrand_array = explode('-', $id_brand);

	$c->listing .= '<li class="nav-menu level1 fly">' . recursive_ul_2($brand_lineage, $idbrand_array) . '</li>';
} else {
	$c->listing .= '<li class="nav-menu level1 fly">' . recursive_ul_2($brand_lineage, null) . '</li>';
}
//list brand

//list price
// $c->listing .= '<li class="nav-menu level1 fly">
//         <a class="nav-link" >Giá cả</a>
//         ' . recursive_ul_price() . '</li>';

echo $c->listing;
?>

        </ul>
    </div>