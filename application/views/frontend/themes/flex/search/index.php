<section class="main-content">
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="product-category-heading">Kết quả tìm kiếm</h1>
                    <div class="toolbar"></div>
                    <div class="products-list">
                        <div class="row" id="bind-home-products">
                            <?php load_element($this->theme_path . 'search/list');?>                            
                        </div>
                        
                    </div>
                    <?php //var_dump($products);?>
                </div>
            </div>
        </div>
    </div>
</section>