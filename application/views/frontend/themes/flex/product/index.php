<section class="main-content">
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-3 hidden-sm hidden-xs"><?php load_element($this->theme_path . '_part/sidebar'); ?></div>
                <div class="col-md-9">
                    <div class="breadcrumb hidden-sm hidden-xs"><?php echo $breadcrumbs; ?></div>
                    <h1 class="product-category-heading"><?php echo $current_category->name; ?></h1>
                    <div class="toolbar"></div>
                    <div class="products-list">
                        <div class="row">
                            <?php load_element($this->theme_path . 'product/list');?>                            
                        </div>
                        <div class="row">
                            <?php echo custom_pagination($this->canonical_url . '/', $total);?>
                        </div>
                    </div>
                    <?php //var_dump($products);?>
                </div>
            </div>
        </div>
    </div>
</section>