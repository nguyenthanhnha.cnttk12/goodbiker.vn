<section class="main-content">
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="product-category-heading">Có thể bạn thích</h1>
                    <div class="toolbar"></div>
                    <div class="products-list">
                        <div class="flex-container" id="bind-home-products">
                            <?php load_element($this->theme_path . 'home/list');?>                            
                        </div>
                        <div class="text-center load-more"><a style="<?php echo (!$next)? 'display:none': '' ?>" id="btn-more-home" onclick="loadmorehome(this)" data-page="2" class="btn btn-default btn-more" >Tải thêm</a></div>
                    </div>
                    <?php //var_dump($products);?>
                </div>
            </div>
        </div>
    </div>
</section>