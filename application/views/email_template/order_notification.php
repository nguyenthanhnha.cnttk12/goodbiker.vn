        <!-- HEADER -->
        <table class="head-wrap" bgcolor="#999999" style="min-width: 80%;max-width:100%;">
            <tr>
                <td></td>
                <td class="header container">

                    <div class="content">
                        <table bgcolor="#999999" style="width:100%;">
                            <tr>
                                <td><img width="100px" src="<?php echo site_url('assets/images/goodbikervn-logo.png') ?>" /></td>
                                <td align="right"><h6 class="collapse">GoodBikerVN</h6></td>
                            </tr>
                        </table>
                    </div>

                </td>
                <td></td>
            </tr>
        </table><!-- /HEADER -->


        <!-- BODY -->
        <table class="body-wrap"  style="min-width: 80%;max-width:100%;">
            <tr>
                <td></td>
                <td class="container" bgcolor="#FFFFFF">

                    <div class="content">
                        <table style="width:100%;">
                            <tr>
                                <td>

                                    <h3>Xin chào,</h3>
                                    <p class="lead">Vừa có một đơn hàng được đặt thành công trên website <a href="https://goodbiker.vn">Goodbiker.vn</a></p>

                                    <!-- A Real Hero (and a real human being) -->
                                    <p>Mã đơn hàng: <?php echo $order['order_code']; ?></p><!-- /hero -->
                                    <p><strong>Thông tin người đặt hàng:</strong></p>
                                    <p>
                                        Tên: <?php echo $address['name'] ?> <br />
                                    Số điện thoại: <?php echo $address['phone'] ?> <br />
                                    Địa chỉ: <?php echo $address['address'] ?> <br />
                                    
                                    </p>
                                    <!-- Callout Panel -->
                                    <p class="callout">
                                        Dưới đây là thông tin chi tiết về đơn hàng (chưa bao gồm phí ship)
                                    </p><!-- /Callout Panel -->

                                    <table class="" border="1" style="border-collapse:collapse;width:100%;border-color: #cccccc;" cellpadding="5" cellspacing="0" >
                                        <tr>
                                            <th>Sản phẩm</th>
                                            <th>Giá</th>
                                            <th>Số lượng</th>
                                            <th>Tạm tính</th>
                                        </tr>
                                        <?php foreach ($cart as $item) {?>
                                        <tr>
                                            <td><img width="120px" class="cart-index-img img-thumbnail" src="<?php echo site_url().$item['image']->path ?>"></td>
                                            <td align="right"><?php echo number_format($item['price'], 0, ',', '.'); ?> ₫</td>
                                            <td align="right"><?php echo $item['qty'] ?></td>
                                            <td align="right"><?php echo number_format($item['price'] * $item['qty'], 0, ',', '.'); ?> ₫</td>
                                        </tr>
                                        <?php } ?>
                                        <tr>
                                            <th colspan="3" align="right">Tổng cộng:</th>
                                            <td align="right"><?php echo number_format($this->cart->total(), 0, ',', '.'); ?> ₫</td>
                                        </tr>
                                    </table>
                                    <p><strong>Hình thức thanh toán:</strong> <?php echo strtoupper($order['payment_method'])?></p>
                                    
                                </td>
                            </tr>
                        </table>
                    </div>

                </td>
                <td></td>
            </tr>
        </table><!-- /BODY -->

        <!-- FOOTER -->
        <table class="footer-wrap"  style="min-width: 80%;max-width:100%;">
            <tr>
                <td></td>
                <td class="container">

                    <!-- content -->
                    <div class="content">
                        <table>
                            <tr>
                                <td align="center">
                                    
                                </td>
                            </tr>
                        </table>
                    </div><!-- /content -->

                </td>
                <td></td>
            </tr>
        </table><!-- /FOOTER -->