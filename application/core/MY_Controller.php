<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public $active_menu = '';
	public $menu = 'dashboard';
	public $page_title = 'Good Biker VN';
	public $page_description = '';
	public $category_lineage;
	public $current_category;
	public $brand_lineage;
	public $current_brand;
	public $products;
	public $canonical_url = NULL;
	public $page_seo;
	public $theme = 'default';
	public $theme_path = '';

	// protected $data = [];
	public function __construct() {
		parent::__construct();
		$this->theme_path = 'frontend/themes/' . $this->theme . '/';
		$this->load->library('carabiner');
		$carabiner_config = array(
			'script_dir' => 'assets/',
			'style_dir' => 'assets/',
			'cache_dir' => 'assets/cache/',
			'base_uri' => base_url(),
			'combine' => FALSE,
			'dev' => TRUE,
		);
		if ($this->uri->segment(1) != 'admin') {
			$this->load->library('cart');
			$this->cart->product_name_rules = '[:print:]';
			if ($this->config->item('maintenance_m[:print:]ode') == TRUE) {
				$output = $this->load->view('maintenance', '', TRUE);
				die($output);
			}
			$this->_loadFrontJSCSS();
			$this->_getCategories();
			$this->_getBrands();
			$this->_getProduct();
		}
		$this->page_seo = new stdClass;

		$this->carabiner->config($carabiner_config);
		$_SESSION['userdata'] = $this->session->userdata();
	}

	public function _is_admin() {
		if (!$this->session->userdata['is_admin']) {
			redirect(site_url('admin/login'));
		}
	}

	public function _renderAdminLayout($view, $data = null) {
		//$data['page_title'] = $this->page_title;
		$this->load->vars($data);
		$this->load->view('admin/_part/header');
		$this->load->view('admin/_part/sidebar', ['menu' => $this->menu]);
		$this->load->view($view);
		$this->load->view('admin/_part/footer');
	}

	public function _renderFrontLayout($view, $data = array(), $include_sidebar = FALSE) {
		$this->load->helper('seo');
		$this->load->model('admin/setting_model');
		$settings = $this->setting_model->getSettings();

		$data['seo'] = json_decode($settings->seo);
		$data['logo'] = $settings->logo;
		$data['hotline'] = $settings->hotline;
		$data['open_time'] = json_decode($settings->open_time);
		$data['social_link'] = json_decode($settings->social_link);
		$data['contact_info'] = json_decode($settings->contact_info);
		$data['category_lineage'] = $this->category_lineage;
		// $data['products'] = $this->products;
		$data['brand_lineage'] = $this->brand_lineage;
		$data['current_category'] = $this->current_category;
		$data['canonical_url'] = $this->canonical_url;
// custom_debug($data);die();
		$data['page_title'] = $this->page_title;

		$this->load->vars($data);
		$this->load->view($this->theme_path . '_part/top');
		$this->load->view($this->theme_path . '_part/header');
		// if($include_sidebar)
		//     $this->load->view('frontend/_part/sidebar');
		$this->load->view($this->theme_path . $view);
		$this->load->view($this->theme_path . '_part/footer');
		$this->load->view($this->theme_path . '_part/bottom');
	}

	public function _loadElement($view, $data = [], $html_output = FALSE) {
		if ($html_output) {
			return $this->load->view($this->theme_path . $view, $data, $html_output);
		} else {
			$this->load->view($this->theme_path . $view, $data, $html_output);
		}
	}

	public function _loadFrontJSCSS() {
		// $this->carabiner->css('css/normalize.css');
		// $this->carabiner->css('bootstrap/css/bootstrap.min.css');
		// $this->carabiner->css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css');
		// $this->carabiner->css('https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css');
		//$this->carabiner->js('js/jquery-1.11.1.min.js');
		//jQuery (necessary for Bootstrap's JavaScript plugins) -->
		$this->carabiner->js('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js');
		$this->carabiner->js('js/jquery-migrate-1.2.1.js');
		//Include all compiled plugins (below), or include individual files as needed -->
		$this->carabiner->js('bootstrap/js/bootstrap.min.js');

		// $this->carabiner->css('js/nivo-slider/themes/default/default.css');
		// $this->carabiner->css('css/animate.min.css');
		// $this->carabiner->css('js/nivo-slider/nivo-slider.css');
		$this->carabiner->js('js/nivo-slider/jquery.nivo.slider.js');
		//Add fancyBox main JS and CSS files -->
		$this->carabiner->js('fancybox/jquery.fancybox.js');
		// $this->carabiner->css('fancybox/jquery.fancybox.css?v=2.1.5');
		//Add Thumbnail helper (this is optional) -->
		// $this->carabiner->css('fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7');
		$this->carabiner->js('fancybox/helpers/jquery.fancybox-thumbs.js');
		// $this->carabiner->css('css/style.css');
		// $this->carabiner->css('css/responsive.css');
		$this->carabiner->js('jquery-confirm/dist/jquery-confirm.min.js');
	}

	public function _getCategories() {
		$this->load->model('admin/category_model');
		//return $this->category_model->getParentCategories();
		$order_by = 'sort';
		$all = $this->category_model->getAllCategories($order_by);
		$category_lineage = [];
		foreach ($all as $key => $category) {
			$category->sub_categories = [];
			$category_lineage[$category->id] = $category;
		}

		foreach ($category_lineage as $key => $val) {
			if ($val->parent_id) {
				$category_lineage[$val->parent_id]->sub_categories[$key] = &$category_lineage[$key];
			}
		}

		$this->category_lineage = $category_lineage;
	}
	public function _getProduct() {
		$this->load->model('admin/product_model');
		$all = $this->product_model->getAllProducts();
		$products = [];
		foreach ($all as $key => $value) {
			$products[$value->id] = $value;
		}
		$this->products = $products;
	}
	public function _getProductfiter() {
		$this->load->model('admin/product_model');
		$all = $this->product_model->getProductRemoveSales();
		$product = [];
		foreach ($all as $key => $value) {
			$product[$value->id] = $value;
		}
		$this->product = $product;
	}

	public function _getSales() {
		$this->load->model('admin/sales_model');
		$all = $this->sales_model->getAllSales();
		$sales_lineage = [];
		foreach ($all as $key => $sales) {
			$sales_lineage[$sales->id] = $sales;
		}
		$this->sales_lineage = $sales_lineage;

	}
	public function _getBrands() {
		$this->load->model('admin/brand_model');
		//return $this->brand_model->getParentCategories();
		$all = $this->brand_model->getAllBrands();
		$brand_lineage = [];
		foreach ($all as $key => $brand) {
			// $brand_lineage[$brand->id]->sub_brands = [];
			$brand_lineage[$brand->id] = $brand;
		}

		// foreach ($category_lineage as $key => $val) {
		//     if ($val->parent_id) {
		//         $category_lineage[$val->parent_id]->sub_categories[$key] = & $category_lineage[$key];
		//     }
		// }

		$this->brand_lineage = $brand_lineage;
	}

	public function _breadcrumbs($current_category) {
		$category_lineage = $this->category_lineage;
		$t = new stdClass;
		$t->breadcrumbs = new stdClass;
		$t->breadcrumbs->current = &$category_lineage[$current_category];
		$t->breadcrumbs->url_mask = site_url($t->breadcrumbs->current->slug);

		$t->breadcrumbs->links = array(
			sprintf('<a href="%1$s">%2$s</a>', $t->breadcrumbs->url_mask, $t->breadcrumbs->current->name
			),
		);

		while ($t->breadcrumbs->current->parent_id) {
			$t->breadcrumbs->current = &$category_lineage[$t->breadcrumbs->current->parent_id];
			array_unshift(
				$t->breadcrumbs->links, sprintf('<a href="%1$s">%2$s</a>', site_url($t->breadcrumbs->current->slug), $t->breadcrumbs->current->name
				)
			);
		}

		$breadcrumbs = '<a href="' . site_url() . '" >Trang chủ</a> > ' . implode(' > ', $t->breadcrumbs->links);
		//custom_debug($breadcrumbs);die();
		return $breadcrumbs;
	}

	public function createPageDescription($description) {
		return "Mua ngay " . $description . ". Mua mũ bảo hiểm online giá tốt, giao hàng tận nơi, thanh toán khi nhận hàng!";
	}

}
