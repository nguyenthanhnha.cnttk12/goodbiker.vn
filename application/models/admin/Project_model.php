<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Projects_model
 *
 * @author TNM Group
 */
class Project_model extends CI_Model {
	private $table = 'projects';

	public function __construct() {
		parent::__construct();
	}

	public function getProjects($page = 0) {
		$total = $this->db->count_all_results($this->table);
		$limit = $this->config->item('admin_per_page');
		$start = ($page <= 1) ? 0 : ($page - 1) * $limit;
		$this->db->limit($limit, $start);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->table);
		$projects = $query->result();
		return ["total" => $total, "projects" => $projects];
	}

	public function getProjectById($id = 0) {
		if ((int) $id > 0) {
			$query = $this->db->get_where($this->table, array('id' => $id));
			return $query->row_array();
		} else {
			return NULL;
		}
	}

	public function insert() {
		$ptype = $this->input->post('ptype');
		$feature_image = $this->input->post('feature_image');
		$slug_vn = create_slug($this->input->post('name_vn'));
		$slug_en = create_slug($this->input->post('name_en'));
		$slug = serialize(["vn" => $slug_vn, "en" => $slug_en]);
		$name = serialize(["vn" => $this->input->post('name_vn'), "en" => $this->input->post('name_en')]);
		$intro = serialize(["vn" => $this->input->post('intro_vn'), "en" => $this->input->post('intro_en')]);
		$overview = serialize(["vn" => $this->input->post('overview_vn'), "en" => $this->input->post('overview_en')]);
		$location = serialize(["vn" => $this->input->post('location_vn'), "en" => $this->input->post('location_en')]);
		$facilities = serialize(["vn" => $this->input->post('facilities_vn'), "en" => $this->input->post('facilities_en')]);
		$progress = serialize(["vn" => $this->input->post('progress_vn'), "en" => $this->input->post('progress_en')]);
		$short_desc = serialize(["vn" => $this->input->post('short_desc_vn'), "en" => $this->input->post('short_desc_en')]);
		$city = serialize(["vn" => $this->input->post('city_vn'), "en" => $this->input->post('city_en')]);
		$price = serialize(["vn" => $this->input->post('price_vn'), "en" => $this->input->post('price_en')]);
		$map = serialize(["latitude" => $this->input->post('latitude'), "longitude" => $this->input->post('longitude'), "zoom" => $this->input->post('zoom')]);
		$video = $this->input->post('video');
		$data = array(
			'name' => $name,
			'slug' => $slug,
			'feature_image' => $feature_image,
			'intro' => $intro,
			'overview' => $overview,
			'map' => $map,
			'location' => $location,
			'facilities' => $facilities,
			'progress' => $progress,
			'video' => $video,
			'ptype' => $ptype,
			'short_desc' => $short_desc,
			'city' => $city,
			'price' => $price,
			'created_time' => date('Y-m-d H:i:s'),
		);
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update() {
		$ptype = $this->input->post('ptype');
		$feature_image = $this->input->post('feature_image');
		$slug_vn = create_slug($this->input->post('name_vn'));
		$slug_en = create_slug($this->input->post('name_en'));
		$slug = serialize(["vn" => $slug_vn, "en" => $slug_en]);
		$name = serialize(["vn" => $this->input->post('name_vn'), "en" => $this->input->post('name_en')]);
		$intro = serialize(["vn" => $this->input->post('intro_vn'), "en" => $this->input->post('intro_en')]);
		$overview = serialize(["vn" => $this->input->post('overview_vn'), "en" => $this->input->post('overview_en')]);
		$location = serialize(["vn" => $this->input->post('location_vn'), "en" => $this->input->post('location_en')]);
		$facilities = serialize(["vn" => $this->input->post('facilities_vn'), "en" => $this->input->post('facilities_en')]);
		$progress = serialize(["vn" => $this->input->post('progress_vn'), "en" => $this->input->post('progress_en')]);
		$short_desc = serialize(["vn" => $this->input->post('short_desc_vn'), "en" => $this->input->post('short_desc_en')]);
		$city = serialize(["vn" => $this->input->post('city_vn'), "en" => $this->input->post('city_en')]);
		$price = serialize(["vn" => $this->input->post('price_vn'), "en" => $this->input->post('price_en')]);
		$map = serialize(["latitude" => $this->input->post('latitude'), "longitude" => $this->input->post('longitude'), "zoom" => $this->input->post('zoom')]);
		$video = $this->input->post('video');
		$data = array(
			'name' => $name,
			'slug' => $slug,

			'intro' => $intro,
			'overview' => $overview,
			'map' => $map,
			'location' => $location,
			'facilities' => $facilities,
			'progress' => $progress,
			'video' => $video,
			'ptype' => $ptype,
			'short_desc' => $short_desc,
			'city' => $city,
			'price' => $price,
			'updated_time' => date('Y-m-d H:i:s'),
		);
		if ($this->input->post('feature_image')) {
			$data['feature_image'] = $this->input->post('feature_image');
		}

		$id = $this->input->post('pid');
		return $this->db->update($this->table, $data, array('id' => $id));
	}

	public function delete($id) {
		$this->db->delete($this->table, array('id' => $id));
	}

}
