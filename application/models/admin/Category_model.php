<?php

/*
 * To change this license header, choose License Headers in Category Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Categorys_model
 *
 * @author TNM Group
 */
class Category_model extends CI_Model {
    private $table = 'categories';

    public function __construct() {
        parent::__construct();
    }

    public function getAllCategories($order_by = 'id', $order_direction = 'ASC') {
        $this->db->order_by($order_by,$order_direction);
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getCategories($page = 0) {  
        $total = $this->db->count_all_results($this->table);
        $limit = $this->config->item('admin_per_page');
        $start = ($page <= 1) ?  0 : ($page - 1)  * $limit;
        $this->db->limit($limit, $start);
        $this->db->order_by('id','ASC');
        $this->db->select('c1.*, c2.name as parent_name');
        $this->db->from($this->table .' c1');
        $this->db->join($this->table .' c2', 'c1.parent_id = c2.id', 'left');
        $query = $this->db->get();
        $categories = $query->result();
        // debug_sql();
        return ["total" => $total, "categories" => $categories];
    }
    

    public function getCategoryById($id = 0) {
        if ((int) $id > 0) {
            $query = $this->db->get_where($this->table, array('id' => $id));
            return $query->row();
        } else {
            return NULL;
        }
    }

    public function getCategoryBySlug($slug = '') {
        if (!empty($slug)) {
            $query = $this->db->get_where($this->table, array('slug' => $slug));
            return $query->row();
        } else {
            return NULL;
        }
    }

    public function insert() {
        $slug = create_slug($this->input->post('name'));
        $name = $this->input->post('name');
        $parent_id = $this->input->post('parent_id');

        $uniq_slug = createUniqueSlug($slug, 'categories', 'slug');
        $data = array(
            'name' => $name,
            'slug' => $uniq_slug,
            'parent_id' => $parent_id
        );
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update() {
        $id = $this->input->post('pid');
        $slug = create_slug($this->input->post('name'));
        $name = $this->input->post('name');
        $parent_id = $this->input->post('parent_id');

        $uniq_slug = createUniqueSlug($slug, 'categories', 'slug', $id);

        $data = array(
            'name' => $name,
            'slug' => $uniq_slug,
            'parent_id' => $parent_id
        );

        
        return $this->db->update($this->table, $data, array('id' => $id));
    }

    public function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
    }

    //-- Frontend function

    public function getParentCategories() {
        $this->db->order_by('sort','ASC');
        $this->db->where('parent_id', 0);
        $query = $this->db->get('categories');
        return $query->result();
    }

}
