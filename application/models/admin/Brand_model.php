<?php

/*
 * To change this license header, choose License Headers in Brand Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Brands_model
 *
 * @author TNM Group
 */
class Brand_model extends CI_Model {
    private $table = 'brands';

    public function __construct() {
        parent::__construct();
    }

    public function getAllBrands() {
        $this->db->order_by('id','ASC');
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getBrands($page = 0) {  
        $total = $this->db->count_all_results($this->table);
        $limit = $this->config->item('admin_per_page');
        $start = ($page <= 1) ?  0 : ($page - 1)  * $limit;
        $this->db->limit($limit, $start);
        $this->db->order_by('id','ASC');
        $this->db->select('c1.*');
        $this->db->from($this->table .' c1');
        $query = $this->db->get();
        $brands = $query->result();
        // debug_sql();
        return ["total" => $total, "brands" => $brands];
    }
    

    public function getBrandById($id = 0) {
        if ((int) $id > 0) {
            $query = $this->db->get_where($this->table, array('id' => $id));
            return $query->row_array();
        } else {
            return NULL;
        }
    }

    public function insert() {
        $slug = create_slug($this->input->post('name'));
        $name = $this->input->post('name');
        $logo = $this->input->post('feature_image');
        
        $data = array(
            'name' => $name,
            'slug' => $slug,
            'logo' => $logo
        );
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update() {
        $slug = create_slug($this->input->post('name'));
        $name = $this->input->post('name');
        
        $data = array(
            'name' => $name,
            'slug' => $slug
        );
        if ($this->input->post('feature_image')) {
            $data['logo'] = $this->input->post('feature_image');
        }

        $id = $this->input->post('pid');
        return $this->db->update($this->table, $data, array('id' => $id));
    }

    public function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
    }

}
