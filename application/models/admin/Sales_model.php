<?php

/**
 * @author thanhnha
 */
class Sales_model extends CI_Model {
	private $table = 'sales';

	function __construct() {
		parent::__construct();
	}
	public function getAllSales() {
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->table);
		return $query->result();
	}
	public function getSales($page = 0) {
		$total = $this->db->count_all_results($this->table);
		$limit = $this->config->item('admin_per_page');
		$start = ($page <= 1) ? 0 : ($page - 1) * $limit;
		$this->db->limit($limit, $start);
		$this->db->order_by('id', 'DESC');
		$this->db->from($this->table . ' s');
		// $this->db->join('product_images im', 'p.id = im.product_id', 'left');
		// $this->db->where('im.featured', 'Yes');
		// $this->db->select('p.*, im.path as image');
		$query = $this->db->get();
		$sales = $query->result();
		// debug_sql();
		return ["total" => $total, "sales" => $sales];
	}
	public function getTotalProductbySales($id = 0) {
		if ((int) $id > 0) {
			$this->db->from('products p');
			$this->db->join('product_sales ps', 'ps.product_id = p.id', 'left');
			$this->db->where('ps.sale_id', $id);
			$this->db->select('count(p.id) as total');
			$query = $this->db->get();
			$products = $query->row();
			if (!$products) {
				show_404();
			}
			return $products;
		} else {
			return 0;
		}
	}
	public function getAllProdBySales($id = 0) {
		if ((int) $id > 0) {
			$this->db->from('products p');
			$this->db->join('product_sales ps', 'ps.product_id = p.id', 'left');
			$this->db->where('ps.sale_id', $id);
			$this->db->select('p.*');
			$query = $this->db->get();

			$products = $query->result();
			$array_id = array();
			foreach ($products as $key => $value) {

				$array_id['$value->id'] = $value->id;
			}
			$total = count($array_id);

			if (!$products) {
				show_404();
			}
			return ["total" => $total, "product" => $products];
		} else {
			return ["total" => 0, "product" => null];
		}
	}
	public function SalesProductByID($id) {
		if ((int) $id > 0) {
			$this->db->from('products p');
			$this->db->join('product_sales ps', 'ps.product_id = p.id', 'left');
			$this->db->where('ps.product_id', $id);
			$query = $this->db->get();

			$products = $query->row();
			if (!$products) {
				return null;
			} else {
				$this->db->from('sales s');
				$this->db->join('product_sales ps', 'ps.sale_id = s.id', 'left');
				$this->db->where('ps.product_id', $id);
				$this->db->select('discount,type');
				$query = $this->db->get();
				$sales = $query->row();

				if ($sales->type == 'percent') {
					$products->price = $products->price - ($products->price * $sales->discount) / 100;
				} else {
					$products->price = $products->price - $sales->discount;
				}

				return $products->price;
			}
		} else {
			return ["product" => null];
		}
	}
	public function getSalesById($id = 0) {
		if ((int) $id > 0) {
			$this->db->select('s.*');
			// $this->db->join('products p', 'p.id = s.id', 'left');
			$this->db->where('s.id', $id);
			$this->db->from($this->table . ' s');
			$query = $this->db->get();
			$sales = $query->row();
			if (!$sales) {
				show_404();
			}
			//get selected product
			$this->db->select('product_id');
			$query = $this->db->get_where('product_sales', array('sale_id' => $id));
			$selected_prod = $query->result();
			$sales->product_id = $selected_prod;

			return $sales;
		} else {
			return NULL;
		}
	}
	public function insert() {
		// custom_debug($_POST); die();

		$title = $this->input->post('title');
		$discount = $this->input->post('discount');
		$type = $this->input->post('type');
		$datestart = $this->input->post('datestart') . " " . $this->input->post('timestart');
		$endstart = $this->input->post('dateend') . " " . $this->input->post('timeend');
		$start_date = date('Y-m-d H:i:s ', strtotime($datestart));
		$end_date = date('Y-m-d H:i:s', strtotime($endstart));
		$now = date('Y-m-d H:i:s');

		$data = array(
			'title' => $title,
			'discount' => $discount,
			'type' => $type,
			'start_date' => $start_date,
			'end_date' => $end_date,
			'created_date' => $now,
		);
		if ($this->input->post('is_active') == 'Yes') {
			$data['is_active'] = 'Yes';
		} else {
			$data['is_active'] = 'No';
		}
		if (!$this->db->insert($this->table, $data)) {
			return false;
		}
		$sales_id = $this->db->insert_id();

		//Add prouct - sales
		$product = $this->input->post('product_id');
		$product_sales = [];
		foreach ($product as $key => $product_id) {
			$product_sales[] = ['product_id' => $product_id,
				'sale_id' => $sales_id,
			];
		}
		$this->db->insert_batch('product_sales', $product_sales);
		return $sales_id;
	}
	public function update() {
		$id = $this->input->post('sid');

		$title = $this->input->post('title');
		$discount = $this->input->post('discount');
		$type = $this->input->post('type');
		$datestart = $this->input->post('datestart') . " " . $this->input->post('timestart');
		$endstart = $this->input->post('dateend') . " " . $this->input->post('timeend');
		$start_date = date('Y-m-d H:i:s ', strtotime($datestart));
		$end_date = date('Y-m-d H:i:s', strtotime($endstart));
		$created_date = date('Y-m-d H:i:s', strtotime($this->input->post('created_date')));

		$now = date('Y-m-d H:i:s');

		$data = array(
			'title' => $title,

			'discount' => $discount,
			'type' => $type,
			'start_date' => $start_date,
			'end_date' => $end_date,
			'created_date' => $now,
			'updated_date' => $now,
		);
		if ($this->input->post('is_active') == 'Yes') {
			$data['is_active'] = 'Yes';
		} else {
			$data['is_active'] = 'No';
		}
		// if (!$this->db->update($this->table, $data)) {
		// 	return false;
		// }

		//Remove
		$this->db->delete('product_sales', ['sale_id' => $id]);
		//Add prouct - category
		$product = $this->input->post('product_id');
		$product_sales = [];
		foreach ($product as $key => $product_id) {
			$product_sales[] = ['sale_id' => $id,
				'product_id' => $product_id,
			];
		}
		$this->db->insert_batch('product_sales', $product_sales);
		$this->db->where('id', $id);
		return $this->db->update($this->table, $data);
	}
	public function delete($id) {
		$this->db->delete('product_sales', ['sale_id' => $id]);
		$this->db->delete('sales', ['id' => $id]);
		$this->db->update($this->table, ['is_active' => 'No'], array('id' => $id));
	}
	public function delete_details($id) {
		$this->db->delete('product_sales', ['product_id' => $id]);
	}
}

?>