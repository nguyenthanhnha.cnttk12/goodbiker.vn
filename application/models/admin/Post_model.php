<?php

/*
 * To change this license header, choose License Headers in Post Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Posts_model
 *
 * @author TNM Group
 */
class Post_model extends CI_Model {
    private $table = 'posts';

    public function __construct() {
        parent::__construct();
    }

    public function getPosts($page = 0) {  
        $total = $this->db->count_all_results($this->table);
        $limit = $this->config->item('admin_per_page');
        $start = ($page <= 1) ?  0 : ($page - 1)  * $limit;
        $this->db->limit($limit, $start);
        $this->db->order_by('id','DESC');
        $query = $this->db->get($this->table);
        $posts = $query->result();
        //echo $this->db->last_query();
        return ["total" => $total, "posts" => $posts];
    }
    

    public function getPostById($id = 0) {
        if ((int) $id > 0) {
            $query = $this->db->get_where($this->table, array('id' => $id));
            return $query->row_array();
        } else {
            return NULL;
        }
    }

    public function insert() {
        $category_id = $this->input->post('category_id');
        $feature_image = $this->input->post('feature_image');
        $slug_vn = create_slug($this->input->post('title_vn'));
        $slug_en = create_slug($this->input->post('title_en'));
        $slug = serialize(["vn" => $slug_vn, "en" => $slug_en ]);
        $title = serialize(["vn" => $this->input->post('title_vn'), "en" => $this->input->post('title_en') ]);
        $content = serialize(["vn" => $this->input->post('content_vn'), "en" => $this->input->post('content_en') ]);
        $short_desc = serialize(["vn" => $this->input->post('short_desc_vn'), "en" => $this->input->post('short_desc_en') ]);
        
        $data = array(
            'title' => $title,
            'slug' => $slug,
            'feature_image' => $feature_image,
            'content' => $content,
            'category_id' => $category_id,
            'short_desc' => $short_desc,
            'created_time' => date('Y-m-d H:i:s'),
        );
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update() {
        $category_id = $this->input->post('category_id');
        $feature_image = $this->input->post('feature_image');
        $slug_vn = create_slug($this->input->post('title_vn'));
        $slug_en = create_slug($this->input->post('title_en'));
        $slug = serialize(["vn" => $slug_vn, "en" => $slug_en ]);
        $title = serialize(["vn" => $this->input->post('title_vn'), "en" => $this->input->post('title_en') ]);
        $content = serialize(["vn" => $this->input->post('content_vn'), "en" => $this->input->post('content_en') ]);
        $short_desc = serialize(["vn" => $this->input->post('short_desc_vn'), "en" => $this->input->post('short_desc_en') ]);
        
        $data = array(
            'title' => $title,
            'slug' => $slug,
            'content' => $content,
            'category_id' => $category_id,
            'short_desc' => $short_desc,
            'updated_time' => date('Y-m-d H:i:s'),
        );
        if ($this->input->post('feature_image')) {
            $data['feature_image'] = $this->input->post('feature_image');
        }

        $id = $this->input->post('pid');
        return $this->db->update($this->table, $data, array('id' => $id));
    }

    public function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
    }

}
