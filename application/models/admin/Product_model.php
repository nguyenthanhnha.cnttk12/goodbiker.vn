<?php

/*
 * To change this license header, choose License Headers in Product Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Products_model
 *
 * @author TNM Group
 */
class Product_model extends CI_Model {

	private $table = 'products';

	public function __construct() {
		parent::__construct();
	}

	public function getAllProducts() {
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function getProducts($page = 0) {
		$total = $this->db->count_all_results($this->table);
		$limit = $this->config->item('admin_per_page');
		$start = ($page <= 1) ? 0 : ($page - 1) * $limit;
		$this->db->limit($limit, $start);
		$this->db->order_by('id', 'DESC');
		$this->db->from($this->table . ' p');
		$this->db->join('product_images im', 'p.id = im.product_id', 'left');
		$this->db->where('im.featured', 'Yes');
		$this->db->select('p.*, im.path as image');
		$query = $this->db->get();
		$products = $query->result();
		// debug_sql();
		return ["total" => $total, "products" => $products];
	}

	public function getProductById($id = 0) {
		if ((int) $id > 0) {
			$this->db->select('p.*, b.name as brand');
			$this->db->join('brands b', 'b.id = p.id', 'left');
			$this->db->where('p.id', $id);
			$this->db->from($this->table . ' p');
			$query = $this->db->get();
			$product = $query->row();
			if (!$product) {
				show_404();
			}
			//get selected categories
			$this->db->select('category_id');
			$query = $this->db->get_where('product_category', array('product_id' => $id));
			$selected_cats = $query->result();
			$product->category_id = $selected_cats;
			//Get product images
			$this->db->select('featured,path');
			$query = $this->db->get_where('product_images', array('product_id' => $id));
			$images = $query->result();
			$product->images = $images;
			return $product;
		} else {
			return NULL;
		}
	}
	public function getProductRemoveSales() {

		$query = $this->db->get('product_sales');
		$product_sales = $query->result();
		$array_id = array();

		foreach ($product_sales as $key => $value) {

			$array_id['$value->product_id'] = $value->product_id;
		}
		// if (!$product_sales) {
		// 	show_404();
		// }
		//get selected categories
		$this->db->where_not_in('id', $array_id);
		$query = $this->db->get('products');
		$products = $query->result();
		if (!$products) {
			return null;
		} else {
			return $products;
		}

	}
	public function SalesProductByID($id) {
		if ((int) $id > 0) {
			$this->db->from('products p');
			$this->db->join('product_sales ps', 'ps.product_id = p.id', 'left');
			$this->db->where('id', $id);
			$query = $this->db->get();

			$products = $query->row();
			if ($products != null) {
				$this->db->from('sales s');
				$this->db->join('product_sales ps', 'ps.sale_id = s.id', 'left');
				$this->db->where('ps.product_id', $id);
				$this->db->select('discount,type');
				$query = $this->db->get();
				$sales = $query->row();
				if ($sales != null) {
					if ($sales->type == 'percent') {
						$products->price = $products->price - ($products->price * $sales->discount) / 100;
					} else {
						$products->price = $products->price - $sales->discount;
					}
				}
			}
			return $products->price;

		} else {
			return ["product" => null];
		}
	}

	public function getProductFeaturedImage($product_id) {

	}

	public function insert() {
		// custom_debug($_POST); die();
		$slug = (!empty($this->input->post('slug'))) ? $this->input->post('slug') : create_slug($this->input->post('name'));
		$name = $this->input->post('name');
		$brand_id = $this->input->post('brand_id');
		$description = $this->input->post('description');
		$price = $this->input->post('price');
		$now = date('Y-m-d H:i:s');

		$uniq_slug = createUniqueSlug($slug, 'products', 'slug');

		$data = array(
			'name' => $name,
			'slug' => $uniq_slug,
			'brand_id' => $brand_id,
			'description' => $description,
			'price' => $price,
			'created_date' => $now,
		);
		if ($this->input->post('is_active') == 'Yes') {
			$data['is_active'] = 'Yes';
		} else {
			$data['is_active'] = 'No';
		}
		if (!$this->db->insert($this->table, $data)) {
			return false;
		}
		$product_id = $this->db->insert_id();
		//Add Product images
		$image = $this->input->post('image');
		$other_imgs = $this->input->post('other_img');
		$product_images = array(['product_id' => $product_id, 'featured' => 'Yes', 'path' => $image]);
		foreach ($other_imgs as $key => $img) {
			$product_images[] = ['product_id' => $product_id,
				'path' => $img,
				'featured' => 'No',
			];
		}
		$this->db->insert_batch('product_images', $product_images);

		//Add prouct - category
		$categories = $this->input->post('category_id');
		$product_category = [];
		foreach ($categories as $key => $category_id) {
			$product_category[] = ['product_id' => $product_id,
				'category_id' => $category_id,
			];
		}
		$this->db->insert_batch('product_category', $product_category);
		return $product_id;
	}

	public function update() {
		$id = $this->input->post('pid');
		$slug = (!empty($this->input->post('slug'))) ? $this->input->post('slug') : create_slug($this->input->post('name'));
		$name = $this->input->post('name');
		$brand_id = $this->input->post('brand_id');
		$description = $this->input->post('description');
		$price = $this->input->post('price');
		$now = date('Y-m-d H:i:s');

		$uniq_slug = createUniqueSlug($slug, 'products', 'slug', $id);

		$data = array(
			'name' => $name,
			'slug' => $uniq_slug,
			'brand_id' => $brand_id,
			'description' => $description,
			'price' => $price,
			'updated_date' => $now,
		);
		if ($this->input->post('is_active') == 'Yes') {
			$data['is_active'] = 'Yes';
		} else {
			$data['is_active'] = 'No';
		}

		//Remove all old images
		$this->db->delete('product_images', ['product_id' => $id]);
		//Add Product images
		$image = $this->input->post('image');
		$other_imgs = $this->input->post('other_img');
		$product_images = array(['product_id' => $id, 'featured' => 'Yes', 'path' => $image]);
		foreach ($other_imgs as $key => $img) {
			$product_images[] = ['product_id' => $id,
				'path' => $img,
				'featured' => 'No',
			];
		}
		$this->db->insert_batch('product_images', $product_images);

		//Remove
		$this->db->delete('product_category', ['product_id' => $id]);
		//Add prouct - category
		$categories = $this->input->post('category_id');
		$product_category = [];
		foreach ($categories as $key => $category_id) {
			$product_category[] = ['product_id' => $id,
				'category_id' => $category_id,
			];
		}
		$this->db->insert_batch('product_category', $product_category);

		return $this->db->update($this->table, $data, array('id' => $id));
	}

	public function delete($id) {
		$this->db->delete('product_category', ['product_id' => $id]);
		$this->db->update($this->table, ['is_active' => 'No'], array('id' => $id));
	}

	//-- front end function
	public function getProductByCategory($category_id, $page = 0, $product_brand, $Sprice, $Eprice) {
		// var_dump($product_brand);
		$subcats = $this->category_lineage[$category_id]->sub_categories;
		$in_categories = [$category_id];
		foreach ($subcats as $key => $val) {
			$in_categories[] = $val->id;
		}
		$this->db->order_by('id', 'DESC');
		$this->db->group_by('c.product_id');
		$this->db->from($this->table . ' p');
		$this->db->join('product_category c', 'p.id = c.product_id', 'left');
		$this->db->join('brands b', 'b.id = p.brand_id', 'left');
		$this->db->join('product_images im', 'p.id = im.product_id', 'left');
		$this->db->where('im.featured', 'Yes');
		if ($product_brand != null) {
			// var_dump($product_brand);

			$this->db->where('p.brand_id', $product_brand);
		}
		if ($Sprice != null) {
			$this->db->where('p.price >=', $Sprice);
		}
		if ($Eprice != null) {
			$this->db->where('p.price <=', $Eprice);
		}
		$this->db->where_in('c.category_id', $in_categories);
		$this->db->select('p.id, p.name, p.price, p.slug, p.code, im.path as image, b.name as brand');
		$total = $this->db->count_all_results();
		$limit = FRONTEND_ITEM_PER_PAGE;
		$start = ($page <= 1) ? 0 : ($page - 1) * $limit;
		$this->db->limit($limit, $start);
		$this->db->order_by('id', 'DESC');
		$this->db->group_by('c.product_id');
		$this->db->from($this->table . ' p');
		$this->db->join('product_category c', 'p.id = c.product_id', 'left');
		$this->db->join('brands b', 'b.id = p.brand_id', 'left');
		$this->db->join('product_images im', 'p.id = im.product_id', 'left');
		$this->db->where('im.featured', 'Yes');
		if ($product_brand != null) {
			$this->db->where('p.brand_id', $product_brand);
		}
		if ($Sprice != null) {
			$this->db->where('p.price >=', $Sprice);
		}
		if ($Eprice != null) {
			$this->db->where('p.price <=', $Eprice);
		}
		$this->db->where_in('c.category_id', $in_categories);
		$this->db->select('p.id, p.name, p.price, p.slug, p.code, im.path as image, b.name as brand');
		$query = $this->db->get();
		$products = $query->result();
//          debug_sql();die();
		return ["total" => $total, "products" => $products];
	}

	public function getFrontProducts($page = 0, $item_per_page = 30) {
		$this->db->select('id');
		$total = $this->db->count_all_results('products');
		$limit = $item_per_page;
		$start = ($page <= 1) ? 0 : ($page - 1) * $limit;
		$this->db->limit($limit, $start);
		$this->db->order_by('rand()');
		$this->db->group_by('p.id');
		$this->db->from($this->table . ' p');
		$this->db->join('brands b', 'b.id = p.id', 'left');
		$this->db->join('product_images im', 'p.id = im.product_id', 'left');
		$this->db->join('product_category c', 'p.id = c.product_id', 'left');
		$this->db->where(array('im.featured' => 'Yes'));
		$this->db->select('p.id, p.name, p.price, p.slug, p.code, im.path as image, b.name as brand, c.category_id');
		$query = $this->db->get();
		$products = $query->result();
//          debug_sql();die();
		return ["total" => $total, "products" => $products];
	}

	public function searchProducts($spm, $page = 1) {
		$keywords = explode(' ', $spm);
		$x = 0;
		$this->db->start_cache();
		foreach ($keywords as $words) {
			$x++;
			if ($x == 1) {
				$this->db->like('p.name ', $words);
				$this->db->or_like('cat.name ', $words);
			} else {
				$this->db->or_like('p.name ', $words);
				$this->db->or_like('cat.name ', $words);
			}
		}
		$this->db->stop_cache();
		$this->db->select('p.id');
//        $this->db->like('name ', $searchterm);
		$this->db->from($this->table . ' p');
		$this->db->join('product_category c', 'p.id = c.product_id', 'left');
		$this->db->join('categories cat', 'c.category_id = cat.id', 'left');
		$this->db->group_by('p.id');
		$total = $this->db->count_all_results();
//        debug_sql();
		$limit = FRONTEND_ITEM_PER_PAGE;
		$start = ($page <= 1) ? 0 : ($page - 1) * $limit;
		$this->db->limit($limit, $start);
		$this->db->order_by('p.id', 'DESC');
		$this->db->group_by('p.id');
		$this->db->from($this->table . ' p');
		$this->db->join('brands b', 'b.id = p.id', 'left');
		$this->db->join('product_images im', 'p.id = im.product_id', 'left');
		$this->db->join('product_category c', 'p.id = c.product_id', 'left');
		$this->db->join('categories cat', 'c.category_id = cat.id', 'left');
		$this->db->where(array('im.featured' => 'Yes'));
//        $this->db->like('p.name ', $searchterm);
		//        $this->db->or_like('cat.name ', $searchterm);
		$this->db->select('p.id, p.name, p.price, p.slug, p.code, im.path as image,p.brand_id, b.name as brand, c.category_id, cat.name as cat_name');
		$query = $this->db->get();
		$products = $query->result();
		$this->db->flush_cache();
//          debug_sql();die();
		return ["total" => $total, "products" => $products];
	}
	public function FilterbyBrand($product, $id, $Sprice, $Eprice) {
		$products = array();
		if ($id != null) {
			if ($Sprice != null && $Eprice != null) {
				foreach ($product as $key => $value) {
					if ($value->brand_id == $id && $value->price >= $Sprice && $value->price <= $Eprice) {
						$products[$value->id] = $value;
					}
				}
			} else {
				foreach ($product as $key => $value) {
					if ($value->brand_id == $id) {
						$products[$value->id] = $value;
					}
				}
			}

		} else {
			$products = $product;
		}
		$total = count($product);
		return ["total" => $total, "products" => $products];

	}

}
