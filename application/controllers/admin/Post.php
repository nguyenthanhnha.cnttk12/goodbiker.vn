<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class Post extends CI_Controller {
    
    private $menu = 'post';

    public function __construct() {
        parent::__construct();
        is_admin();
        $this->load->helper(array('form', 'html', 'file', 'path'));
        $this->load->library('form_validation');
        $this->load->model('admin/post_model');
    }

    public function index($page = 0) {
        if ($this->uri->segment(5) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(5);
        }
        $data['data'] = $this->post_model->getPosts($page);
        $this->load->view('admin/include/header');
        $this->load->view('admin/include/sidebar', ['menu'=> $this->menu]);
        $this->load->view('admin/post/index', $data);
        $this->load->view('admin/include/footer');
    }

    public function add() {        
        if ($this->input->post('save')) {
            //var_dump($_POST);die();
            $this->save();
        } else {
            $this->load->view('admin/include/header');
            $this->load->view('admin/include/sidebar', ['menu'=> $this->menu]);
            $this->load->view('admin/post/add');
            $this->load->view('admin/include/footer');
        }
    }

    public function edit($id) {
        $data = $this->post_model->getPostById($id);
        $data['controller'] = $this;
        $data['pid'] = $this->input->post('pid');// $id;
        if ($this->input->post('save')) {
            $this->save();
        } else {
            $this->load->view('admin/include/header');
            $this->load->view('admin/include/sidebar', ['menu'=> $this->menu]);
            $this->load->view('admin/post/edit', $data);
            $this->load->view('admin/include/footer');
        }
    }

    public function save() {
        $this->form_validation->set_rules('feature_image', 'Ảnh đại diện', 'callback_handle_feature_upload');
        $data = $this->input->post();

        $id = (int) $data["pid"];
        //If have page id parameter then update page, else add new page
        if ($id > 0) {

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/include/header');
                $this->load->view('admin/include/sidebar', ['menu'=> $this->menu]);
                $this->load->view('admin/post/add', $data);
                $this->load->view('admin/include/footer');
            } else {
                $this->post_model->update();
                $this->session->set_flashdata('msg', 'Tin được cập nhật thành công!');
                redirect('/admin/post/edit/' . $id);
            }
        } else {
            
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/include/header');
                $this->load->view('admin/include/sidebar', ['menu'=> $this->menu]);
                $this->load->view('admin/post/add', $data);
                $this->load->view('admin/include/footer');
            } else {
                //var_dump($data);die();
                $id = $this->post_model->insert();
                $this->session->set_flashdata('msg', 'Tin mới đã được thêm vào dữ liệu!');
                redirect('/admin/post/');
            }
        }
    }

    public function delete($id) {
        if ((int) $id > 0) {
            $this->post_model->delete($id);
        }
        $this->session->set_flashdata('msg', 'Tin đã được xóa!');
        redirect('/admin/post');
    }
    
    public function action() {
        $val = $this->input->post('val');
        $action = $this->input->post('hidAction');
        if($action == 'delete') {
            $in = implode(',', $val);
            $this->db->where("id in ($in)");
            $this->db->delete('posts');
        }
        redirect('/admin/post');
    }

    function handle_feature_upload() {
        if (isset($_FILES['feature_image']) && !empty($_FILES['feature_image']['name'])) {
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = './media/feature_image/';
            $config['allowed_types'] = 'gif|jpg|png';
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('feature_image')) {
                // set a $_POST value for 'image' that we can use later
                $upload_data = $this->upload->data();
                $this->load->helper('image');
                resize_image($upload_data['full_path'], 600, 400);
                $_POST['feature_image'] = "media/feature_image/".$upload_data['file_name'];
                return true;
            } else {
                // possibly do some clean up ... then throw an error
                $this->form_validation->set_message('handle_feature_upload', $this->upload->display_errors());
                return false;
            }
        } else {
            // throw an error because nothing was uploaded
            //$this->form_validation->set_message('handle_banner_upload', "You must upload an image!");
            return true;
        }
    }

    public function getBase64Image($image) {
        $config['image_library'] = 'gd2';
        $config['source_image'] = set_realpath('media/' . $image);
        $imageData = base64_encode(file_get_contents(set_realpath('media/' . $image)));
        $this->load->library('image_lib', $config);
        $src = 'data: ' . $this->image_lib->mime_type . ';base64,' . $imageData;
        return $src;
    }

}
