<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class Brand extends MY_Controller {
    
    // public $menu = 'brand';
    // public $page_title = 'Hãng';

    public function __construct() {
        parent::__construct();
        $this->_is_admin();
        $this->menu = 'brand';
        $this->page_title = 'Hãng';
        $this->load->helper(array('form', 'html', 'file', 'path'));
        $this->load->library('form_validation');
        $this->load->model('admin/brand_model');
    }

    public function index() {

        if ($this->uri->segment(5) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(5);
        }
        $data['data'] = $this->brand_model->getBrands($page);
        //var_dump($data);die();
        $this->_renderAdminLayout('admin/brand/index', $data);
    }

    public function add() {
        if ($this->input->post('save')) {
            //var_dump($_POST);die();
            $this->_save();
        } else {
            $data['controller'] = $this;
            $this->_renderAdminLayout('admin/brand/add');
        }
    }

    public function edit($id) {
        $data = $this->brand_model->getBrandById($id);
        $data['controller'] = $this;
        $data['pid'] = $this->input->post('pid'); // $id;
        if ($this->input->post('save')) {
            $this->_save();
        } else {
            $this->_renderAdminLayout('admin/brand/edit', $data);
        }
    }

    public function _save() {
        $this->form_validation->set_rules('feature_image', 'Logo', 'callback_handle_feature_upload');
        $data = $this->input->post();

        $id = (int) $data["pid"];
        //If have page id parameter then update page, else add new page
        if ($id > 0) {
            $this->brand_model->update();
            $this->session->set_flashdata('msg', 'Hãng được cập nhật thành công!');
            redirect('/admin/brand/edit/' . $id);
        } else {
            //var_dump($data);die();
            $id = $this->brand_model->insert();
            $this->session->set_flashdata('msg', 'Hãng mới đã được thêm vào dữ liệu!');
            redirect('/admin/brand/');
        }
    }

    public function delete($id) {
        if ((int) $id > 0) {
            $this->brand_model->delete($id);
        }
        $this->session->set_flashdata('msg', 'Hãng đã được xóa!');
        redirect('/admin/brand');
    }

    public function action() {
        $val = $this->input->post('val');
        $action = $this->input->post('hidAction');
        if ($action == 'delete') {
            $in = implode(',', $val);
            $this->db->where("id in ($in)");
            $this->db->delete('brands');
        }
        if ($action == 'sorting') {
            $sort = $this->input->post('sort');
            foreach ($val as $key => $value) {
                $this->db->update('brands', ["sort" => $sort[$key]], ["id" => $value]);
            }
        }
        redirect('/admin/brand');
    }

    public function _getAllBrands() {
        return $this->brand_model->getAllBrands();
    }

    function _handle_feature_upload() {
        if (isset($_FILES['feature_image']) && !empty($_FILES['feature_image']['name'])) {
            //$config['encrypt_name'] = TRUE;
            $config['upload_path'] = './uploads/images/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['file_name'] = create_slug($_FILES['feature_image']['name']);
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('feature_image')) {
                // set a $_POST value for 'image' that we can use later
                $upload_data = $this->upload->data();
                $_POST['feature_image'] = "uploads/images/".$upload_data['file_name'];
                return true;
            } else {
                // possibly do some clean up ... then throw an error
                $this->form_validation->set_message('handle_feature_upload', $this->upload->display_errors());
                return false;
            }
        } else {
            // throw an error because nothing was uploaded
            //$this->form_validation->set_message('handle_banner_upload', "You must upload an image!");
            return true;
        }
    }

}
