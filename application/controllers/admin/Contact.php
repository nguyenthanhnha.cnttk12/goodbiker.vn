<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class Contact extends CI_Controller {
    
    private $menu = 'contact';

    public function __construct() {
        parent::__construct();
        is_admin();
        $this->load->helper(array('form', 'html', 'file', 'path'));
        $this->load->library('form_validation');
        $this->load->model('admin/contact_model');
    }

    public function index($page = 0) {
        if ($this->uri->segment(5) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(5);
        }
        $data['data'] = $this->contact_model->getContacts($page);
        $this->load->view('admin/include/header');
        $this->load->view('admin/include/sidebar', ['menu'=> $this->menu]);
        $this->load->view('admin/contact/index', $data);
        $this->load->view('admin/include/footer');
    }
    
    public function delete($id) {
        if ((int) $id > 0) {
            $this->contact_model->delete($id);
        }
        $this->session->set_flashdata('msg', 'Tin đã được xóa!');
        redirect('/admin/contact');
    }
    
    public function action() {
        $val = $this->input->post('val');
        $action = $this->input->post('hidAction');
        if($action == 'delete') {
            $in = implode(',', $val);
            $this->db->where("id in ($in)");
            $this->db->delete('posts');
        }
        redirect('/admin/contact');
    }

}
