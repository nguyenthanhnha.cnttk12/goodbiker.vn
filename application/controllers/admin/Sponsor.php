<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class Sponsor extends CI_Controller {
    
    private $menu = 'sponsor';

    public function __construct() {
        parent::__construct();
        is_admin();
        $this->load->helper(array('form', 'html', 'file', 'path'));
        $this->load->library('form_validation');
        $this->load->model('admin/sponsor_model');
    }

    public function index($page = 0) {
        if ($this->uri->segment(5) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(5);
        }
        $data['data'] = $this->sponsor_model->getSponsors($page);
        $this->load->view('admin/include/header');
        $this->load->view('admin/include/sidebar', ['menu'=> $this->menu]);
        $this->load->view('admin/sponsor/index', $data);
        $this->load->view('admin/include/footer');
    }

    public function add() {        
        if ($this->input->post('save')) {
            //var_dump($_POST);die();
            $this->save();
        } else {
            $this->load->view('admin/include/header');
            $this->load->view('admin/include/sidebar', ['menu'=> $this->menu]);
            $this->load->view('admin/sponsor/add');
            $this->load->view('admin/include/footer');
        }
    }

    public function edit($id) {
        $data = $this->sponsor_model->getSponsorById($id);
        $data['controller'] = $this;
        $data['pid'] = $this->input->post('pid');// $id;
        if ($this->input->post('save')) {
            $this->save();
        } else {
            $this->load->view('admin/include/header');
            $this->load->view('admin/include/sidebar', ['menu'=> $this->menu]);
            $this->load->view('admin/sponsor/edit', $data);
            $this->load->view('admin/include/footer');
        }
    }

    public function save() {
        $this->form_validation->set_rules('feature_image', 'Ảnh đại diện', 'callback_handle_feature_upload');
        $data = $this->input->post();

        $id = (int) $data["pid"];
        //If have page id parameter then update page, else add new page
        if ($id > 0) {

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/include/header');
                $this->load->view('admin/include/sidebar', ['menu'=> $this->menu]);
                $this->load->view('admin/sponsor/add', $data);
                $this->load->view('admin/include/footer');
            } else {
                $this->sponsor_model->update();
                $this->session->set_flashdata('msg', 'Tin được cập nhật thành công!');
                redirect('/admin/sponsor/edit/' . $id);
            }
        } else {
            
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/include/header');
                $this->load->view('admin/include/sidebar', ['menu'=> $this->menu]);
                $this->load->view('admin/sponsor/add', $data);
                $this->load->view('admin/include/footer');
            } else {
                //var_dump($data);die();
                $id = $this->sponsor_model->insert();
                $this->session->set_flashdata('msg', 'Tin mới đã được thêm vào dữ liệu!');
                redirect('/admin/sponsor/');
            }
        }
    }

    public function delete($id) {
        if ((int) $id > 0) {
            $this->sponsor_model->delete($id);
        }
        $this->session->set_flashdata('msg', 'Tin đã được xóa!');
        redirect('/admin/sponsor');
    }
    
    public function action() {
        $val = $this->input->post('val');
        $action = $this->input->post('hidAction');
        if($action == 'delete') {
            $in = implode(',', $val);
            $this->db->where("id in ($in)");
            $this->db->delete('sponsors');
        }
        redirect('/admin/sponsor');
    }

    function handle_feature_upload() {
        if (isset($_FILES['feature_image']) && !empty($_FILES['feature_image']['name'])) {
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = './media/sponsor_image/';
            $config['allowed_types'] = 'gif|jpg|png';
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('feature_image')) {
                // set a $_POST value for 'image' that we can use later
                $upload_data = $this->upload->data();
                $_POST['feature_image'] = "media/sponsor_image/".$upload_data['file_name'];
                return true;
            } else {
                // possibly do some clean up ... then throw an error
                $this->form_validation->set_message('handle_feature_upload', $this->upload->display_errors());
                return false;
            }
        } else {
            // throw an error because nothing was uploaded
            //$this->form_validation->set_message('handle_banner_upload', "You must upload an image!");
            return true;
        }
    }

}
