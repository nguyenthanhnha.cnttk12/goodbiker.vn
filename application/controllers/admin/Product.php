<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class Product extends MY_Controller {
    
    // public $menu = 'product';
    // public $page_title = 'Sản phẩm';

    public function __construct() {
        parent::__construct();
        $this->_is_admin();
        $this->menu = 'product';
        $this->page_title = 'Sản phẩm';
        $this->load->helper(array('form', 'html', 'file', 'path'));
        $this->load->library('form_validation');
        $this->load->model('admin/product_model');
        $this->_getCategories();
    }

    public function index() {

        if ($this->uri->segment(5) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(5);
        }
        $data['data'] = $this->product_model->getProducts($page);
        //var_dump($data);die();
        $this->_renderAdminLayout('admin/product/index', $data);
    }

    public function add() {
        $this->carabiner->js('ckeditor/ckeditor.js');
        $this->carabiner->js('js/ckeditor-loader.js');
        if ($this->input->post('save')) {
            //var_dump($_POST);die();
            $this->_save();
        } else {
            $data['category_lineage'] = $this->category_lineage;
            // var_dump($this->category_lineage);die();
            $data['controller'] = $this;
            $this->_renderAdminLayout('admin/product/add', $data);
        }
    }

    public function edit($id) {
        $this->carabiner->js('ckeditor/ckeditor.js');
        $this->carabiner->js('js/ckeditor-loader.js');
        $data['category_lineage'] = $this->category_lineage;
        $data['product'] = $this->product_model->getProductById($id);
        $data['controller'] = $this;
        $data['pid'] = $this->input->post('pid'); // $id;
        if ($this->input->post('save')) {
            $this->_save();
        } else {
            $this->_renderAdminLayout('admin/product/edit', $data);
        }
    }

    public function _save() {
        $data = $this->input->post();

        $id = (int) $data["pid"];
        //If have page id parameter then update page, else add new page
        if ($id > 0) {
            $this->product_model->update();
            $this->session->set_flashdata('msg', 'Sản phẩm được cập nhật thành công!');
            redirect('/admin/product/edit/' . $id);
        } else {
            //var_dump($data);die();
            $id = $this->product_model->insert();
            $this->session->set_flashdata('msg', 'Sản phẩm mới đã được thêm vào dữ liệu!');
            redirect('/admin/product/');
        }
    }

    public function delete($id) {
        if ((int) $id > 0) {
            $this->product_model->delete($id);
        }
        $this->session->set_flashdata('msg', 'Sản phẩm đã được xóa!');
        redirect('/admin/product');
    }

    public function action() {
        $val = $this->input->post('val');
        $action = $this->input->post('hidAction');
        if ($action == 'delete') {
            $in = implode(',', $val);
            $this->db->where("id in ($in)");
            $this->db->delete('products');
        }
        if ($action == 'sorting') {
            $sort = $this->input->post('sort');
            foreach ($val as $key => $value) {
                $this->db->update('products', ["sort" => $sort[$key]], ["id" => $value]);
            }
        }
        redirect('/admin/product');
    }

    public function _getAllproducts() {
        return $this->product_model->getAllproducts();
    }

}
