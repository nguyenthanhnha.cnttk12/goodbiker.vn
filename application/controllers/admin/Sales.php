<?php
/**
 *
 */
class Sales extends My_controller {

	public function __construct() {
		parent::__construct();
		$this->_is_admin();
		$this->menu = 'sales';
		$this->page_title = 'Khuyễn mãi';
		$this->load->helper(array('form', 'html', 'file', 'path'));
		$this->load->library('form_validation');
		$this->load->model('admin/sales_model');
		$this->_getSales();
		$this->_getProduct();
		$this->_getProductfiter();
	}

	public function index() {

		if ($this->uri->segment(5) === FALSE) {
			$page = 0;
		} else {
			$page = $this->uri->segment(5);
		}
		$data['data'] = $this->sales_model->getSales($page);
		//var_dump($data);die();
		$this->_renderAdminLayout('admin/sales/index', $data);
	}
	public function details($id) {
		$data['data'] = $this->sales_model->getAllProdBySales($id);
		$sales = $this->sales_model->getSalesById($id);
		$data['title'] = $sales->title;
		$data['id'] = $id;
		$this->_renderAdminLayout('admin/sales/details', $data);
	}
	public function add() {
		$this->carabiner->js('ckeditor/ckeditor.js');
		$this->carabiner->js('js/ckeditor-loader.js');
		if ($this->input->post('save')) {
			//var_dump($_POST);die();
			$this->_save();
		} else {
			$data['product'] = $this->product;
			// var_dump($this->category_lineage);die();
			$data['controller'] = $this;
			$this->_renderAdminLayout('admin/sales/add', $data);
		}
	}

	public function edit($id) {
		$this->carabiner->js('ckeditor/ckeditor.js');
		$this->carabiner->js('js/ckeditor-loader.js');
		$data['product'] = $this->product;
		$data['products'] = $this->products;
		$data['sales'] = $this->sales_model->getSalesById($id);
		$data['controller'] = $this;
		$data['sid'] = $this->input->post('sid'); // $id;
		if ($this->input->post('save')) {
			$this->_save();
		} else {
			$this->_renderAdminLayout('admin/sales/edit', $data);
		}
	}

	public function _save() {
		$data = $this->input->post();

		$id = (int) $data["sid"];
		//If have page id parameter then update page, else add new page
		if ($id > 0) {
			$this->sales_model->update();
			$this->session->set_flashdata('msg', 'Sản phẩm được cập nhật thành công!');
			redirect('/admin/sales/edit/' . $id);
		} else {
			//var_dump($data);die();
			$id = $this->sales_model->insert();
			$this->session->set_flashdata('msg', 'Khuyễn mãi mới đã được thêm vào dữ liệu!');
			redirect('/admin/sales/');
		}
	}

	public function delete($id) {
		if ((int) $id > 0) {
			$this->sales_model->delete($id);
		}
		$this->session->set_flashdata('msg', 'Sản phẩm đã được xóa!');
		redirect('/admin/sales');
	}
	public function delete_details($id_sales, $id_product) {
		if ((int) $id_product > 0) {
			$this->sales_model->delete_details($id_product);
		}
		$this->session->set_flashdata('msg', 'Sản phẩm đã được xóa!');
		redirect('/admin/sales/details/' . $id_sales);
	}
	public function details_add($id) {
		$this->carabiner->js('ckeditor/ckeditor.js');
		$this->carabiner->js('js/ckeditor-loader.js');
		$data['product'] = $this->product;
		$data['id'] = $id;
		$data['sales'] = $this->sales_model->getSalesById($id);
		$data['title'] = $data['sales']->title;
		$data['controller'] = $this;
		$data['sid'] = $this->input->post('sid'); // $id;
		if ($this->input->post('save')) {
			$this->_save();
		} else {
			$this->_renderAdminLayout('admin/sales/add_details', $data);
		}
	}
	public function action() {
		$val = $this->input->post('val');
		$action = $this->input->post('hidAction');
		if ($action == 'delete') {
			$in = implode(',', $val);
			$this->db->where("id in ($in)");
			$this->db->delete('sales');
		}
		if ($action == 'sorting') {
			$sort = $this->input->post('sort');
			foreach ($val as $key => $value) {
				$this->db->update('sales', ["sort" => $sort[$key]], ["id" => $value]);
			}
		}
		redirect('/admin/sales');
	}

	public function _getAllproducts() {
		return $this->product_model->getAllproducts();
	}

}
?>