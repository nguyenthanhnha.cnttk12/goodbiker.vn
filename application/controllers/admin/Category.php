<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class Category extends MY_Controller {
    
    // public $menu = 'category';
    // public $page_title = 'Danh mục';

    public function __construct() {
        parent::__construct();
        $this->_is_admin();
        $this->menu = 'category';
        $this->page_title = 'Danh mục';
        $this->load->helper(array('form', 'html', 'file', 'path'));
        $this->load->library('form_validation');
        $this->load->model('admin/category_model');
    }

    public function index() {

        if ($this->uri->segment(5) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(5);
        }
        $data['data'] = $this->category_model->getCategories($page);
        //var_dump($data);die();
        $this->_renderAdminLayout('admin/category/index', $data);
    }

    public function add() {
        if ($this->input->post('save')) {
            //var_dump($_POST);die();
            $this->_save();
        } else {
            $data['controller'] = $this;
            $this->_renderAdminLayout('admin/category/add');
        }
    }

    public function edit($id) {
        $data['category'] = $this->category_model->getCategoryById($id);
        $data['controller'] = $this;
        $data['pid'] = $this->input->post('pid'); // $id;
        if ($this->input->post('save')) {
            $this->_save();
        } else {
            $this->_renderAdminLayout('admin/category/edit', $data);
        }
    }

    public function _save() {
        $data = $this->input->post();

        $id = (int) $data["pid"];
        //If have page id parameter then update page, else add new page
        if ($id > 0) {
            $this->category_model->update();
            $this->session->set_flashdata('msg', 'Danh mục được cập nhật thành công!');
            redirect('/admin/category/edit/' . $id);
        } else {
            //var_dump($data);die();
            $id = $this->category_model->insert();
            $this->session->set_flashdata('msg', 'Danh mục mới đã được thêm vào dữ liệu!');
            redirect('/admin/category/');
        }
    }

    public function delete($id) {
        if ((int) $id > 0) {
            $this->category_model->delete($id);
        }
        $this->session->set_flashdata('msg', 'Danh mục đã được xóa!');
        redirect('/admin/category');
    }

    public function action() {
        $val = $this->input->post('val');
        $action = $this->input->post('hidAction');
        if ($action == 'delete') {
            $in = implode(',', $val);
            $this->db->where("id in ($in)");
            $this->db->delete('categories');
        }
        if ($action == 'sorting') {
            $sort = $this->input->post('sort');
            foreach ($val as $key => $value) {
                $this->db->update('categories', ["sort" => $sort[$key]], ["id" => $value]);
            }
        }
        redirect('/admin/category');
    }

    public function _getAllCategories() {
        return $this->category_model->getAllCategories();
    }

}
