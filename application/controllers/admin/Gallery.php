<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class Gallery extends CI_Controller {

    private $menu = 'gallery';

    public function __construct() {
        parent::__construct();
        is_admin();
        $this->load->helper(array('form', 'html', 'file', 'path'));
        $this->load->library('form_validation');
        $this->load->model('admin/gallery_model');
    }

    public function index($page = 0) {
        if ($this->uri->segment(5) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(5);
        }
        $data['data'] = $this->gallery_model->getGallerys($page);
        $this->load->view('admin/include/header');
        $this->load->view('admin/include/sidebar', ['menu' => $this->menu]);
        $this->load->view('admin/gallery/index', $data);
        $this->load->view('admin/include/footer');
    }

    public function add() {
        if ($this->input->post('save')) {
            //var_dump($_POST);die();
            $this->save();
        } else {
            $this->load->view('admin/include/header');
            $this->load->view('admin/include/sidebar', ['menu' => $this->menu]);
            $this->load->view('admin/gallery/add');
            $this->load->view('admin/include/footer');
        }
    }

    public function edit($id) {
        $data = $this->gallery_model->getGalleryById($id);
        $data['controller'] = $this;
        $data['pid'] = $this->input->post('pid'); // $id;
        if ($this->input->post('save')) {
            $this->save();
        } else {
            $this->load->view('admin/include/header');
            $this->load->view('admin/include/sidebar', ['menu' => $this->menu]);
            $this->load->view('admin/gallery/edit', $data);
            $this->load->view('admin/include/footer');
        }
    }

    public function save() {
        $data = $this->input->post();
//        custom_debug($_FILES);
//        die();
        $id = (int) $data["pid"];
        //If have page id parameter then update page, else add new page
        if ($id > 0) {
            $this->gallery_model->update();
            $this->session->set_flashdata('msg', 'Tin được cập nhật thành công!');
            redirect('/admin/gallery/edit/' . $id);
        } else {
//            var_dump($data);
//            die();
            $id = $this->gallery_model->insert();
            $this->session->set_flashdata('msg', 'Tin mới đã được thêm vào dữ liệu!');
            redirect('/admin/gallery/');
        }
    }

    public function delete($id) {
        if ((int) $id > 0) {
            $this->gallery_model->delete($id);
        }
        $this->session->set_flashdata('msg', 'Tin đã được xóa!');
        redirect('/admin/gallery');
    }

    public function action() {
        $val = $this->input->post('val');
        $action = $this->input->post('hidAction');
        if ($action == 'delete') {
            $in = implode(',', $val);
            $this->db->where("id in ($in)");
            $this->db->delete('gallery');
        }
        redirect('/admin/gallery');
    }

    public function delete_image() {
        $id = $this->input->post('id');
        $file = FCPATH . $this->input->post('uri');
        $this->db->trans_start();
        $this->db->where(['id' => $id]);
        $this->db->delete('gallery_images');
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            die(json_encode(['error' => TRUE]));
        } else {
            if (file_exists($file) && $this->input->post('uri') != '') {
                unlink($file);
            }
            die(json_encode(['error' => FALSE]));
        }
    }

}
