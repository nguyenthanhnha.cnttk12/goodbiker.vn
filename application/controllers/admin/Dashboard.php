<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    // public $menu = 'dashboard';
    // public $page_title = 'Admin Dashboard';

    public function __construct() {
        parent::__construct();
        $this->_is_admin();
        $this->menu = 'dashboard';
        $this->page_title = 'Admin Dashboard';
        $this->load->helper(array('form', 'html', 'file', 'path', 'secure'));
    }

    public function index() {
        //$this->hits_model->adminView();
        //custom_debug($this->hits_model->getTotalHits());
        //$data['pages'] = $this->pages_model->getPages();
        
        $data['page_title'] = $this->config->item('site_name') . ' | ' . 'Pages';
        $this->_renderAdminLayout('admin/dashboard');
    }

}
