<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author FitDNN
 */
class Cart extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->menu = 'cart';
        $this->load->model('admin/product_model');
        $this->load->helper('form');
        $this->page_title = "Giỏ hàng";
    }

    public function index() {
        $data = [];
        $this->_renderFrontLayout('cart/index', $data);
    }

    public function update() {
        $rowid = $this->input->post('rowid');
        $qty = $this->input->post('qty');
        $data = array(
            'rowid' => $rowid,
            'qty' => $qty
        );
        // die(json_encode($this->cart->contents()));
        //$this->cart->update($data);
        if (!$this->cart->update($data)) {
            die(json_encode(['error' => 'Có lỗi xảy ra']));
        } else {
            if ($qty > 0) {
                $cart = $this->cart->contents();
                $current_item = $cart[$rowid];
                $item_subtotal = number_format($current_item['subtotal'], 0, ',', '.') . " ₫";
            } else {
                $item_subtotal = 0;
            }
            $total = number_format($this->cart->total(), 0, ',', '.') . " ₫";
            die(json_encode(['qty' => $this->cart->total_items(), 'subtotal' => $item_subtotal, 'total' => $total]));
        }
    }

    public function checkout() {
        $this->page_title = "Thanh toán";
        $this->carabiner->css('js/plugins/icheck/skins/square/orange.css');
        $this->carabiner->js('js/plugins/icheck/icheck.min.js');
        $this->carabiner->js('js/validator.js');
        $this->carabiner->js('js/checkout.js');
        if ($this->input->post('phone')) {
            //var_dump($this->input->post());
            $this->load->library('form_validation');

            $this->form_validation->set_rules('name', 'Tên', 'required', array('required' => 'Vui lòng nhập %s.'));
            $this->form_validation->set_rules('phone', 'Số điện thoại', 'required', array('required' => 'Vui lòng nhập %s.'));
            $this->form_validation->set_rules('address', 'Địa chỉ nhận hàng', 'required', array('required' => 'Vui lòng nhập %s.'));
            $this->form_validation->set_rules('payment_method', 'Hình thức thanh toán', 'required', array('required' => 'Vui lòng chọn %s.'));

            if ($this->form_validation->run() == FALSE) {
                show_error('Thông tin không hợp lệ!', 406, 'Không hợp lệ');
                exit();
            } else {
                $this->_createOrder();
            }
            die();
        }
        $data = [];
        $this->_renderFrontLayout('cart/checkout', $data);
    }

    public function _createOrder() {
        $order = [];
        $order['user_id'] = 0;
        $order['order_date'] = date('Y-m-d H:i:s');
        $order['payment_method'] = $this->input->post('payment_method');
        $order['status'] = 'pending';
        $order['order_code'] = 'GB' . time();
        $order_error_msg = CREATE_ORDER_ERROR_MSG;
        if (!$this->db->insert('orders', $order)) {
            show_error($order_error_msg, 500, ERROR_HEADING);
            die();
        }
        $order_id = $this->db->insert_id();
        $order_details = [];
        foreach ($this->cart->contents() as $items) {
            $order_item = array('order_id' => $order_id,
                'product_id' => $items['id'],
                'qty' => $items['qty'],
                'price' => $items['price']);
            $order_details[] = $order_item;
        }
        if (!$this->db->insert_batch('order_detail', $order_details)) {
            $this->db->delete('orders', ['id' => $order_id]);
            show_error($order_error_msg, 500, ERROR_HEADING);
            die();
        }
        $address = array('name' => $this->input->post('name'), 'phone' => $this->input->post('phone'), 'address' => $this->input->post('address'));
        $shipping = array('order_id' => $order_id, 'address' => serialize($address));
        if (!$this->db->insert('shipping', $shipping)) {
            $this->db->delete('orders', ['id' => $order_id]);
            $this->db->delete('order_detail', ['order_id' => $order_id]);
            show_error($order_error_msg, 500, ERROR_HEADING);
            die();
        }
        
        //Send notification email for admin
        $data = array('order' => $order, 'cart' => $this->cart->contents() , 'address' => $address);
        $message = $this->load->view('email_template/order_notification', $data, TRUE);
//        die($message);
        $this->_sendOrderEmail($message, $order['order_code']);
        $this->cart->destroy();
        redirect(site_url('thankyou'));
    }

    public function _sendOrderEmail($message = '', $order_code) {
        $this->load->library('email'); 
        $this->email->from('no-reply@goodbiker.vn', 'GoodBikerVN');
        $this->email->to('orders@goodbiker.vn');
        $this->email->subject('Thông báo đơn hàng '. $order_code);
        
        $this->email->message($message);

        $this->email->send();
    }

}
