	<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class Product extends MY_Controller {

	// public $menu = 'product';
	// public $page_title = 'Sản phẩm';

	public function __construct() {
		parent::__construct();
		$this->menu = 'product';
		$this->page_title = '';
		$this->load->model('admin/product_model');
		$this->load->model('admin/category_model');
	}

	public function index($slug, $page = 0) {

		$current_category = $this->category_model->getCategoryBySlug($slug);
		if (!$current_category) {
			show_404();
		}
		$this->canonical_url = site_url($current_category->slug);
		$this->page_title = $current_category->name;
		$this->page_description = $this->createPageDescription($current_category->name);
		$this->page_seo->title = $current_category->name;
		$this->page_seo->description = $this->createPageDescription($current_category->name);
		$this->page_seo->type = 'product';
		$this->page_seo->meta_url = $this->canonical_url;

		$data = $this->product_model->getProductByCategory($current_category->id, $page, null, null, null);
		$data['breadcrumbs'] = $this->_breadcrumbs($current_category->id);
		$this->current_category = $current_category;
		$this->_renderFrontLayout('product/index', $data, TRUE);

	}

	public function detail($slug, $product_id, $category_id) {
		if ($product_id > 0) {
			$product = $this->product_model->getProductById($product_id);

			if ($product != null) {
				// custom_debug($product);die();
				$this->carabiner->css('smoothproducts/css/smoothproducts.css');
				$this->carabiner->js('smoothproducts/js/smoothproducts.min.js');
				$this->carabiner->js('js/product-page.js');
				// $referrer = $_SERVER['HTTP_REFERER'];
				//echo $referrer;
				// $parts = parse_url($referrer);
				// $path = $parts['path'];
				$current_category = $this->category_model->getCategoryById($category_id);
				$this->current_category = $current_category;

				$featured_image = $other_images = NULL;

				foreach ($product->images as $key => $image) {
					if ($image->featured == 'Yes') {
						$featured_image = $image;
					} else {
						$other_images[] = $image;
					}
				}
				// var_dump($product->images);die();
				$category_ids = [];
				foreach ($product->category_id as $key => $value) {
					$category_ids[] = $value->category_id;
				}
				$this->canonical_url = ($category_id == max($category_ids)) ? NULL : getProductUrl($product, $this->category_lineage[max($category_ids)]->id);

				$this->page_title = $product->name;
				$this->page_description = $this->createPageDescription($product->name);
				$this->page_seo->title = $product->name;
				$this->page_seo->description = 'product';
				$this->page_seo->meta_url = $this->canonical_url;
				$this->page_seo->meta_img = site_url($featured_image->path);
				$data['breadcrumbs'] = $this->_breadcrumbs($current_category->id);
				$data['featured_image'] = $featured_image;
				$data['other_images'] = $other_images;
				$data['product'] = $product;
				// var_dump($data);
				// exit();
				$this->_renderFrontLayout('product/detail', $data, TRUE);
			} else {
				show_404();
			}
		} else {
			show_404();
		}
	}

	public function addtocart() {

		$product_id = $this->input->post('product_id');
		if ($product_id > 0) {
			$product = $this->product_model->getProductById($product_id);
			if ($product) {
				$product = $this->product_model->getProductById($product_id);
				$data['id'] = $product->id;
				$data['name'] = $product->name;
				$data['qty'] = 1;
				$data['price'] = $product->price;
				$featured_image = NULL;

				foreach ($product->images as $key => $image) {
					if ($image->featured == 'Yes') {
						$featured_image = $image;
						break;
					}
				}
				$data['image'] = $featured_image;

				// $this->cart->destroy();
				$rowId = $this->cart->insert($data);
				//                $serializeCart = serialize($this->cart->contents());
				//                $this->input->set_cookie('goodbiker_cart_cookie', $serializeCart,36000);
				if ($rowId) {

					// $response = ['image' => $featured_image, 'cart' => $this->cart->contents()];
					$response = $this->_loadElement('cart/cart_alert', ['product' => $product], TRUE);
					die(json_encode(['html' => $response, 'qty' => $this->cart->total_items()]));
				} else {
					$response = ['error' => 'Có lỗi xảy ra! Vui lòng thử lại.'];
					die(json_encode($response));
				}
			} else {
				show_404();
			}
		} else {
			show_404();
		}
	}
	public function index_brand($slug) {
		$data_pust = array();
		$id_brand = $this->input->get('id_brand');
		$Sprice = $this->input->get('Startprice');
		$Eprice = $this->input->get('Endprice');
		$totol_pust = 0;
		$current_category = $this->category_model->getCategoryBySlug($slug);
		if (!$current_category) {
			show_404();
		}
		if (isset($id_brand)) {
			if ($id_brand != "") {
				$idbrand_array = explode('-', $id_brand);
				for ($i = 0; $i < count($idbrand_array); $i++) {
					if ((isset($Sprice)) && (isset($Eprice))) {
						$data = $this->product_model->getProductByCategory($current_category->id, 1, $idbrand_array[$i], $Sprice, $Eprice);
						$data_pust = array_merge($data['products'], $data_pust);
						$totol_pust = $totol_pust + $data['total'];
					} else {
						$data = $this->product_model->getProductByCategory($current_category->id, 1, $idbrand_array[$i], null, null);
						$data_pust = array_merge($data['products'], $data_pust);
						$totol_pust = $totol_pust + $data['total'];
					}
				}
				$data['products'] = $data_pust;
				$data['total'] = $totol_pust;
			} else {

				if ((isset($Sprice)) && (isset($Eprice))) {
					$data = $this->product_model->getProductByCategory($current_category->id, 1, null, $Sprice, $Eprice);
					$data_pust = array_merge($data['products'], $data_pust);
					$totol_pust = $totol_pust + $data['total'];
				} else {
					$data = $this->product_model->getProductByCategory($current_category->id, 1, null, null, null);
					$data_pust = array_merge($data['products'], $data_pust);
					$totol_pust = $totol_pust + $data['total'];
				}

				$data['products'] = $data_pust;
				$data['total'] = $totol_pust;
			}
		}if (!isset($id_brand)) {

			if ((isset($Sprice)) && (isset($Eprice))) {
				$data = $this->product_model->getProductByCategory($current_category->id, 1, null, $Sprice, $Eprice);
				$data_pust = $data['products'];
				$totol_pust = $totol_pust + $data['total'];
			} else {
				$data = $this->product_model->getProductByCategory($current_category->id, 1, null, null, null);
				$data_pust = $data['products'];
				$totol_pust = $totol_pust + $data['total'];
			}
			$data['products'] = $data_pust;
			$data['total'] = $totol_pust;
		}

		$this->canonical_url = site_url($current_category->slug);
		$this->page_title = $current_category->name;
		$this->page_description = $this->createPageDescription($current_category->name);
		$this->page_seo->title = $current_category->name;
		$this->page_seo->description = $this->createPageDescription($current_category->name);
		$this->page_seo->type = 'product';
		$this->page_seo->meta_url = $this->canonical_url;

		$data['breadcrumbs'] = $this->_breadcrumbs($current_category->id);
		$this->current_category = $current_category;
		$this->_renderFrontLayout('product/index', $data, TRUE);
	}
	public function findtobrand() {
		$slug = $this->input->post('slug');
		$data_pust = array();
		$id_brand = $this->input->post('id');
		$totol_pust = 0;
		$current_category = $this->category_model->getCategoryBySlug($slug);
		if (!$current_category) {
			show_404();
		}
		if (isset($id_brand)) {
			$idbrand_array = explode('-', $id_brand);
			for ($i = 0; $i < count($idbrand_array); $i++) {
				$data = $this->product_model->getProductByCategory($current_category->id, 1, $idbrand_array[$i], null, null);
				$data_pust = array_merge($data['products'], $data_pust);
				$totol_pust = $totol_pust + $data['total'];
			}
			$data['products'] = $data_pust;
			$data['total'] = $totol_pust;

		} else {
			$data = $this->product_model->getProductByCategory($current_category->id, 1, null, null, null);
		}

		// die(json_encode(['html' => $data]));
		$data['current_category'] = $current_category;
		$response = $this->_loadElement('product/list', $data, TRUE);
		if ($response != null) {
			die(json_encode(['html' => $response]));
		} else {
			die(json_encode(['html' => '<h3>không tìm thấy dữ liệu</h3>']));
		}
	}

	public function findtoprice() {
		$totol_pust = 0;
		$data_pust = array();
		$slug = $this->input->post('slug');
		$Sprice = $this->input->post('Sprice');
		$Eprice = $this->input->post('Eprice');
		$id_brand = $this->input->post('id');
		$current_category = $this->category_model->getCategoryBySlug($slug);
		if (!$current_category) {
			show_404();
		}
		if (isset($id_brand)) {
			if ($id_brand != "") {
				$idbrand_array = explode('-', $id_brand);
				for ($i = 0; $i < count($idbrand_array); $i++) {
					if ((isset($Sprice)) && (isset($Eprice))) {
						$data = $this->product_model->getProductByCategory($current_category->id, 1, $idbrand_array[$i], $Sprice, $Eprice);
						$data_pust = array_merge($data['products'], $data_pust);
						$totol_pust = $totol_pust + $data['total'];
					} else {
						$data = $this->product_model->getProductByCategory($current_category->id, 1, $idbrand_array[$i], null, null);
						$data_pust = array_merge($data['products'], $data_pust);
						$totol_pust = $totol_pust + $data['total'];
					}
				}
				$data['products'] = $data_pust;
				$data['total'] = $totol_pust;
			} else {

				if ((isset($Sprice)) && (isset($Eprice))) {
					$data = $this->product_model->getProductByCategory($current_category->id, 1, null, $Sprice, $Eprice);
					$data_pust = array_merge($data['products'], $data_pust);
					$totol_pust = $totol_pust + $data['total'];
				} else {
					$data = $this->product_model->getProductByCategory($current_category->id, 1, null, null, null);
					$data_pust = array_merge($data['products'], $data_pust);
					$totol_pust = $totol_pust + $data['total'];
				}

				$data['products'] = $data_pust;
				$data['total'] = $totol_pust;
			}
		}if (!isset($id_brand)) {

			if ((isset($Sprice)) && (isset($Eprice))) {
				$data = $this->product_model->getProductByCategory($current_category->id, 1, null, $Sprice, $Eprice);
			} else {
				$data = $this->product_model->getProductByCategory($current_category->id, 1, null, null, null);
			}
		}

		$data['current_category'] = $current_category;
		$response = $this->_loadElement('product/list', $data, TRUE);
		if ($response != null) {
			die(json_encode(['html' => $response]));
		} else {
			die(json_encode(['html' => '<h3>không tìm thấy dữ liệu</h3>']));
		}

	}
	public function search() {

	}

}
