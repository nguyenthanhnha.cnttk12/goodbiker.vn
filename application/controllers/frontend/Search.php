<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class Search extends MY_Controller {

	// public $menu = 'product';
	// public $page_title = 'Sản phẩm';

	public function __construct() {
		parent::__construct();
		$this->menu = 'search';
		$this->page_title = 'Tìm kiếm sản phẩm';
		$this->load->model('admin/product_model');
		$this->load->model('admin/category_model');
	}

	public function index() {
		$spm = $this->input->get('spm');
		$data = $this->product_model->searchProducts($spm);
		$this->page_seo->type = 'website';
		$this->page_seo->meta_url = $this->canonical_url;
		$this->_renderFrontLayout('search/index', $data);
	}
	public function index_search() {
		$totol_pust = 0;
		$data_pust = array();
		$spm = $this->input->get('spm');
		$id_brand = $this->input->get('id_brand');
		$Sprice = $this->input->get('Startprice');
		$Eprice = $this->input->get('Endprice');
		$data = $this->product_model->searchProducts($spm);
		if (!$dataAll) {
			show_404();
			$data = null;
		} else {
			if (isset($id_brand)) {
				if ($id_brand != "") {
					$idbrand_array = explode('-', $id_brand);
					for ($i = 0; $i < count($idbrand_array); $i++) {
						if ((isset($Sprice)) && (isset($Eprice))) {
							$data = $this->product_model->FilterbyBrand($dataAll['products'], $idbrand_array[$i], $Sprice, $Eprice);
							$data_pust = array_merge($data['products'], $data_pust);
							$totol_pust = $totol_pust + $data['total'];
						} else {
							$data = $this->product_model->FilterbyBrand($dataAll['products'], $idbrand_array[$i], null, null);
							$data_pust = array_merge($data['products'], $data_pust);
							$totol_pust = $totol_pust + $data['total'];
						}
					}
					$data['products'] = $data_pust;
					$data['total'] = $totol_pust;
				} else {

					if ((isset($Sprice)) && (isset($Eprice))) {
						$data = $this->product_model->FilterbyBrand($dataAll['products'], null, $Sprice, $Eprice);
						$data_pust = array_merge($data['products'], $data_pust);
						$totol_pust = $totol_pust + $data['total'];
					} else {
						$data = $this->product_model->FilterbyBrand($dataAll['products'], null, null, null);
						$data_pust = array_merge($data['products'], $data_pust);
						$totol_pust = $totol_pust + $data['total'];
					}

					$data['products'] = $data_pust;
					$data['total'] = $totol_pust;
				}
			}if (!isset($id_brand)) {

				if ((isset($Sprice)) && (isset($Eprice))) {
					$data = $this->product_model->FilterbyBrand($dataAll['products'], null, $Sprice, $Eprice);
					$data_pust = $data['products'];
					$totol_pust = $totol_pust + $data['total'];
				} else {
					$data = $this->product_model->FilterbyBrand($dataAll['products'], null, null, null);
					$data_pust = $data['products'];
					$totol_pust = $totol_pust + $data['total'];
				}
				$data['products'] = $data_pust;
				$data['total'] = $totol_pust;
			}
		}

		$this->page_seo->type = 'website';
		$this->page_seo->meta_url = $this->canonical_url;
		$this->_renderFrontLayout('search/index', $data);
	}
	public function filtertobrandsearch() {
		$totol_pust = 0;
		$data_pust = array();
		$spm = $this->input->post('spm');
		$id_brand = $this->input->post('id');
		$Sprice = $this->input->post('Sprice');
		$Eprice = $this->input->post('Eprice');
		$dataAll = $this->product_model->searchProducts($spm);
		if (!$dataAll) {
			show_404();
			$data = null;
		} else {
			if (isset($id_brand)) {
				if ($id_brand != "") {
					$idbrand_array = explode('-', $id_brand);
					for ($i = 0; $i < count($idbrand_array); $i++) {
						if ((isset($Sprice)) && (isset($Eprice))) {
							$data = $this->product_model->FilterbyBrand($dataAll['products'], $idbrand_array[$i], $Sprice, $Eprice);
							$data_pust = array_merge($data['products'], $data_pust);
							$totol_pust = $totol_pust + $data['total'];
						} else {
							$data = $this->product_model->FilterbyBrand($dataAll['products'], $idbrand_array[$i], null, null);
							$data_pust = array_merge($data['products'], $data_pust);
							$totol_pust = $totol_pust + $data['total'];
						}
					}
					$data['products'] = $data_pust;
					$data['total'] = $totol_pust;
				} else {

					if ((isset($Sprice)) && (isset($Eprice))) {
						$data = $this->product_model->FilterbyBrand($dataAll['products'], null, $Sprice, $Eprice);
						$data_pust = array_merge($data['products'], $data_pust);
						$totol_pust = $totol_pust + $data['total'];
					} else {
						$data = $this->product_model->FilterbyBrand($dataAll['products'], null, null, null);
						$data_pust = array_merge($data['products'], $data_pust);
						$totol_pust = $totol_pust + $data['total'];
					}

					$data['products'] = $data_pust;
					$data['total'] = $totol_pust;
				}
			}if (!isset($id_brand)) {

				if ((isset($Sprice)) && (isset($Eprice))) {
					$data = $this->product_model->FilterbyBrand($dataAll['products'], null, $Sprice, $Eprice);
					$data_pust = $data['products'];
					$totol_pust = $totol_pust + $data['total'];
				} else {
					$data = $this->product_model->FilterbyBrand($dataAll['products'], null, null, null);
					$data_pust = $data['products'];
					$totol_pust = $totol_pust + $data['total'];
				}
				$data['products'] = $data_pust;
				$data['total'] = $totol_pust;
			}
		}
		// die(json_encode(['html' => $data]));
		// $this->page_seo->type = 'website';
		// $this->page_seo->meta_url = $this->canonical_url;
		$response = $this->_loadElement('search/list', $data, TRUE);
		if ($response != null) {
			die(json_encode(['html' => $response]));
		} else {
			die(json_encode(['html' => 'No Product!']));
		}

	}
}